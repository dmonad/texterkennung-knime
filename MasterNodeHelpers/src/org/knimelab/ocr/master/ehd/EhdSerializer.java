package org.knimelab.ocr.master.ehd;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.codec.binary.Base64InputStream;
import org.apache.commons.codec.binary.Base64OutputStream;

/**
 * Utility class that serializes the edge histogram data. <br/> <br />
 * <b> EHD Format description:</b> <br/>
 * <pre>
 * double[][][] ehd = ...
 * Access the information via ehd[gridX][gridY][bin]
 * gridX - x position of the selected grid
 * gridY - y position of the selected grid
 * bin - contains the gradient orientation information for each bin
 * </pre>
 *
 * <b><i>NOTE:</b></i> You need apache commons-codec for this to work! <br />
 * (<a href="http://commons.apache.org/proper/commons-codec/">http://commons.apache.org/proper/commons-codec/</a>)
 *
 * @author Michael Hennings <michael.hennings@rwth-aachen.de>
 */
public final class EhdSerializer {

  /**
   * Converts the given array to a BASE64-encoded string (using Java's native
   * serialization capabilities).
   *
   * @param array
   * @return
   */
  public static String serialize(double[][][] array) {
    String retVal = "";
    try {
      ByteArrayOutputStream bas = new ByteArrayOutputStream();
      ObjectOutputStream oos = new ObjectOutputStream(
              new Base64OutputStream(bas, true, 0, null));
      oos.writeObject(array);
      oos.close();
      retVal = bas.toString("UTF-8");
    } catch (IOException ex) {
      Logger.getLogger(EhdSerializer.class.getName()).log(Level.SEVERE, null,
              ex);
    }
    return retVal;
  }

  /**
   * Decodes the given BASE64-encoded string and returns it as
   * <code>double[][][]</code>.
   *
   * @param string BASE64-encoded string
   * @return
   */
  public static double[][][] deserialize(String string) {
    double[][][] retVal = null;
    try {
      ObjectInputStream ois = new ObjectInputStream(
              new Base64InputStream(
              new ByteArrayInputStream(string.getBytes("UTF-8"))));
      Object readObject = ois.readObject();
      ois.close();
      retVal = (double[][][]) readObject;
    } catch (IOException | ClassNotFoundException ex) {
      Logger.getLogger(EhdSerializer.class.getName()).log(Level.SEVERE, null,
              ex);
    }
    return retVal;
  }
}