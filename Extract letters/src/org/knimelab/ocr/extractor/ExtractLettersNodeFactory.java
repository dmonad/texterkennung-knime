package org.knimelab.ocr.extractor;

import org.knime.core.node.NodeDialogPane;
import org.knime.core.node.NodeFactory;
import org.knime.core.node.NodeView;

/**
 * <code>NodeFactory</code> for the "ExtractLetters" Node.
 * This node extracts letters of a PNG-Image and gives back all the letters as PNG-Images
 *
 * @author Knime Lab Group
 */
public class ExtractLettersNodeFactory 
        extends NodeFactory<ExtractLettersNodeModel> {

    /**
     * {@inheritDoc}
     */
    @Override
    public ExtractLettersNodeModel createNodeModel() {
        return new ExtractLettersNodeModel();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getNrNodeViews() {
        return 0;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public NodeView<ExtractLettersNodeModel> createNodeView(final int viewIndex,
            final ExtractLettersNodeModel nodeModel) {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasDialog() {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public NodeDialogPane createNodeDialogPane() {
        return new ExtractLettersNodeDialog();
    }

}

