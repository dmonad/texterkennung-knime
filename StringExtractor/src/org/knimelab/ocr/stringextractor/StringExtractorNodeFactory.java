package org.knimelab.ocr.stringextractor;

import org.knime.core.node.NodeDialogPane;
import org.knime.core.node.NodeFactory;
import org.knime.core.node.NodeView;

/**
 * <code>NodeFactory</code> for the "StringExtractor" Node.
 * 
 *
 * @author Team kNN
 */
public class StringExtractorNodeFactory 
        extends NodeFactory<StringExtractorNodeModel> {

    /**
     * {@inheritDoc}
     */
    @Override
    public StringExtractorNodeModel createNodeModel() {
        return new StringExtractorNodeModel();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getNrNodeViews() {
        return 0;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public NodeView<StringExtractorNodeModel> createNodeView(final int viewIndex,
            final StringExtractorNodeModel nodeModel) {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasDialog() {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public NodeDialogPane createNodeDialogPane() {
        return new StringExtractorNodeDialog();
    }

}

