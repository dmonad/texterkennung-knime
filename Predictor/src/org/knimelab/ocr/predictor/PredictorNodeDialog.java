package org.knimelab.ocr.predictor;

import javax.swing.ListSelectionModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.knime.core.data.DataTableSpec;
import org.knime.core.data.DoubleValue;
import org.knime.core.data.IntValue;
import org.knime.core.data.StringValue;
import org.knime.core.data.collection.ListDataValue;
import org.knime.core.data.image.png.PNGImageValue;
import org.knime.core.node.InvalidSettingsException;
import org.knime.core.node.NodeSettingsRO;
import org.knime.core.node.NotConfigurableException;
import org.knime.core.node.defaultnodesettings.DefaultNodeSettingsPane;
import org.knime.core.node.defaultnodesettings.DialogComponentBoolean;
import org.knime.core.node.defaultnodesettings.DialogComponentColumnNameSelection;
import org.knime.core.node.defaultnodesettings.DialogComponentNumber;
import org.knime.core.node.defaultnodesettings.DialogComponentStringListSelection;
import org.knime.core.node.defaultnodesettings.DialogComponentStringSelection;
import org.knime.core.node.util.ColumnFilter;
import org.knime.core.node.util.DataValueColumnFilter;
import org.knime.core.node.util.DefaultStringIconOption;
import org.knime.core.node.util.StringIconOption;

/**
 * <code>NodeDialog</code> for the "Predictor" Node.
 * It compares images
 *
 * This node dialog derives from {@link DefaultNodeSettingsPane} which allows
 * creation of a simple dialog with standard components. If you need a more 
 * complex dialog please derive directly from 
 * {@link org.knime.core.node.NodeDialogPane}.
 * 
 * @author Kevin Jahns
 */
public class PredictorNodeDialog extends DefaultNodeSettingsPane {

	PredictorNodeSettings settings = new PredictorNodeSettings();
	
    /**
     * New pane for configuring Predictor node dialog.
     * This is just a suggestion to demonstrate possible default dialog
     * components.
     */
    @SuppressWarnings("unchecked")
	protected PredictorNodeDialog() {
        super();
        
        this.createNewGroup("Predictor settings");
        // Add Scale Images By - Option
        this.addDialogComponent(
        		new DialogComponentStringListSelection(
        				settings.scaleImagesBy
        			, "Scale images by", new StringIconOption[]{
        					new DefaultStringIconOption("Height"),
        					new DefaultStringIconOption("Width"),
        					new DefaultStringIconOption("Don't scale")
        				}, ListSelectionModel.MULTIPLE_INTERVAL_SELECTION, true, 3)
        );
      
        // Scale up or down
        this.addDialogComponent(new DialogComponentBoolean(settings.scaleUp, "scale up (otherwise scale down)"));
        
        // Scale method       	
        this.addDialogComponent(
        	new DialogComponentStringSelection(settings.scaleFunction, "Scale Function",
        			new String[]{
        			"Area averaging",
    				"Default" ,
    				"Fast",
    				"Replicate",
    				"Smooth"	
        	})
        );
        
        // Similarity function 
        this.addDialogComponent(
        	new DialogComponentStringListSelection(settings.similarityFunction, 
        	"Similarity function", 
        	new StringIconOption[]{
        		new DefaultStringIconOption("p=2 Distance function"),
				new DefaultStringIconOption("Cosine similarity (to white)"),
				new DefaultStringIconOption("Cosine similarity (to black)")
        	}	
        	, ListSelectionModel.SINGLE_SELECTION, true, 3));
        
        
        this.createNewGroup("Select columns");
        
		DataValueColumnFilter filter = new DataValueColumnFilter(PNGImageValue.class);
        DataValueColumnFilter filterString = new DataValueColumnFilter(StringValue.class);
        this.addDialogComponent(
        		new DialogComponentColumnNameSelection(settings.learnerImageName, "Learner image column", 
        				0, (ColumnFilter) filter ));
        this.addDialogComponent(
        		new DialogComponentColumnNameSelection(settings.dataImageName, "Data image column", 
        				1, (ColumnFilter) filter ));
        this.addDialogComponent(
        		new DialogComponentColumnNameSelection(settings.learnerImageLetter, "Letter representing lerner image", 
        				0, (ColumnFilter) filterString ));
        
        // Give back scaled images 
        this.addDialogComponent(
        		new DialogComponentBoolean(
        				settings.giveBackScaledImages,
        				"Give back scaled images")
        );
        
        

        class AddProjectionChangeListener implements ChangeListener{

        	int buttonCount;
        	boolean isDisplayed = false;
        	PredictorNodeDialog context;
        	
        	protected AddProjectionChangeListener(int buttonCount, PredictorNodeDialog context){
        		super();
        		this.buttonCount = buttonCount;
        		this.context = context;
        		settings.addProjectionLearningModel[buttonCount].setEnabled(false);
        		settings.addProjectionDataModel[buttonCount].setEnabled(false);
        		settings.distanceStepModel[buttonCount].setEnabled(false);
        	}
			@Override
			public void stateChanged(ChangeEvent e) {	
				if(buttonCount +1 < settings.maxProjectionModels){
					settings.addProjectionButtonModel[buttonCount+1].setEnabled(settings.addProjectionButtonModel[buttonCount].getBooleanValue());
				}
				settings.addProjectionDataModel[buttonCount].setEnabled(settings.addProjectionButtonModel[buttonCount].getBooleanValue());
				settings.addProjectionLearningModel[buttonCount].setEnabled(settings.addProjectionButtonModel[buttonCount].getBooleanValue());
				settings.distanceStepModel[buttonCount].setEnabled(settings.addProjectionButtonModel[buttonCount].getBooleanValue());				
			}
        	
        }


        this.createNewTab("Learning Data Projection");

        this.addDialogComponent(
        		new DialogComponentNumber(settings.takeAtLeastRelative, 
        				"How many Elements to take at least relatively to learning input", 0.001)
        		);
        this.addDialogComponent(
        		new DialogComponentNumber(settings.minimumPropability,
        				"Minimum Propability", 0.1)
        		);

        
        for(int i=0; i<settings.maxProjectionModels; i++){
        	if(i != 0 && i % 3 == 0 )
        		this.createNewTab("Learning Data Projection #" + ((int)(i/3)));
			this.createNewGroup("Projection " + i);
        	this.addDialogComponent(
		        	new DialogComponentBoolean( settings.addProjectionButtonModel[i], "Add a projection function")
		        	);
	        this.addDialogComponent(
	        		new DialogComponentColumnNameSelection(settings.addProjectionLearningModel[i] 
	        				, "Learning Port", 0, false, DoubleValue.class, IntValue.class, ListDataValue.class)
	        		);
	        this.addDialogComponent(
	        		new DialogComponentColumnNameSelection(settings.addProjectionDataModel[i] 
	        				, "Data Port", 1, false, DoubleValue.class, IntValue.class, ListDataValue.class)
	        		);
	        this.addDialogComponent(
	        		new DialogComponentNumber(settings.distanceStepModel[i], "Stepsize", 1)
	        		);
	        settings.addProjectionButtonModel[i].addChangeListener(
        			new AddProjectionChangeListener(i,this)
        			);
        }    	
        
        settings.addProjectionButtonModel[0].setEnabled(true);
        
    }
    
    @Override
    protected void loadSettingsFrom(final NodeSettingsRO settings,
            final DataTableSpec[] specs) throws NotConfigurableException
    {
        try
        {
            this.settings.loadSettings(settings);
        }
        catch (InvalidSettingsException ex)
        {
            // Ignore errors and use the defaults
        }
    }
    
}

