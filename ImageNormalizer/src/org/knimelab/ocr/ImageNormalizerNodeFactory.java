package org.knimelab.ocr;

import org.knime.core.node.NodeDialogPane;
import org.knime.core.node.NodeFactory;
import org.knime.core.node.NodeView;

/**
 * <code>NodeFactory</code> for the "ImageNormalizer" Node.
 * 
 *
 * @author Kevin Jahns
 */
public class ImageNormalizerNodeFactory 
        extends NodeFactory<ImageNormalizerNodeModel> {

    /**
     * {@inheritDoc}
     */
    @Override
    public ImageNormalizerNodeModel createNodeModel() {
        return new ImageNormalizerNodeModel();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getNrNodeViews() {
        return 0;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public NodeView<ImageNormalizerNodeModel> createNodeView(final int viewIndex,
            final ImageNormalizerNodeModel nodeModel) {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasDialog() {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public NodeDialogPane createNodeDialogPane() {
        return new ImageNormalizerNodeDialog();
    }

}

