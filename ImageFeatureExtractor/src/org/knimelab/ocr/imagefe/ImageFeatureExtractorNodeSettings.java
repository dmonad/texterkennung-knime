package org.knimelab.ocr.imagefe;

import org.knime.core.node.InvalidSettingsException;
import org.knime.core.node.NodeSettingsRO;
import org.knime.core.node.NodeSettingsWO;
import org.knime.core.node.defaultnodesettings.SettingsModelBoolean;
import org.knime.core.node.defaultnodesettings.SettingsModelInteger;
import org.knime.core.node.defaultnodesettings.SettingsModelString;

public class ImageFeatureExtractorNodeSettings
{
    public final SettingsModelString imageColumn =
    		new SettingsModelString("Image column", "Image");
    
    public final SettingsModelString mode =
    		new SettingsModelString("Mode", "Angular");
    
    public final SettingsModelInteger bins =
    		new SettingsModelInteger("Number", 10);
    
    public final SettingsModelBoolean useBlackCenter =
    		new SettingsModelBoolean("Use black center", false);
    
    public String getImageColumn()
    {
    	return imageColumn.getStringValue();
    }
    
    public String getMode()
    {
    	return mode.getStringValue();
    }
    
    public int getNumber()
    {
    	return bins.getIntValue();
    }
    
    public boolean isUsingBlackCenter()
    {
    	return useBlackCenter.getBooleanValue();
    }

    public void saveSettings(final NodeSettingsWO settings)
    {
    	imageColumn.saveSettingsTo(settings);    
    	mode.saveSettingsTo(settings);      
    	bins.saveSettingsTo(settings);    
    	useBlackCenter.saveSettingsTo(settings);      		
    }

    public void loadSettings(final NodeSettingsRO settings)
            throws InvalidSettingsException
    {
    	imageColumn.loadSettingsFrom(settings);
    	mode.loadSettingsFrom(settings);
    	bins.loadSettingsFrom(settings);
    	useBlackCenter.loadSettingsFrom(settings);
    }

    public void validateSettings(final NodeSettingsRO settings)
            throws InvalidSettingsException
    {
    	imageColumn.validateSettings(settings);
    	mode.validateSettings(settings);
    	bins.validateSettings(settings);
    	useBlackCenter.validateSettings(settings);
    }

}
