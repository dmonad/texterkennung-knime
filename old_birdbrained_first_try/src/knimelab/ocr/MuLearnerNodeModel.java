package knimelab.ocr;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.knime.core.data.DataCell;
import org.knime.core.data.DataColumnSpec;
import org.knime.core.data.DataColumnSpecCreator;
import org.knime.core.data.DataRow;
import org.knime.core.data.DataTableSpec;
import org.knime.core.data.RowKey;
import org.knime.core.data.collection.CollectionCellFactory;
import org.knime.core.data.collection.ListCell;
import org.knime.core.data.def.DefaultRow;
import org.knime.core.data.def.DoubleCell;
import org.knime.core.data.def.IntCell;
import org.knime.core.data.def.StringCell;
import org.knime.core.node.BufferedDataContainer;
import org.knime.core.node.BufferedDataTable;
import org.knime.core.node.CanceledExecutionException;
import org.knime.core.node.ExecutionContext;
import org.knime.core.node.ExecutionMonitor;
import org.knime.core.node.InvalidSettingsException;
import org.knime.core.node.NodeModel;
import org.knime.core.node.NodeSettingsRO;
import org.knime.core.node.NodeSettingsWO;
import org.knime.core.node.defaultnodesettings.SettingsModelDouble;


/**
 * This is the model implementation of MuLearner.
 * 
 *
 * @author KNN
 */
public class MuLearnerNodeModel extends NodeModel
{
	public static final SettingsModelDouble TRESHOLD_SETTING = new SettingsModelDouble("treshold", 255);
	
    protected MuLearnerNodeModel()
    {
        super(1, 1);
    }
    
    private final HashMap<String, Letter> letters = new HashMap<String, Letter>();
    private final HashMap<String, Integer> counts = new HashMap<String, Integer>();

    @Override
    protected BufferedDataTable[] execute(final BufferedDataTable[] inData,
            final ExecutionContext exec) throws Exception {

        DataColumnSpec[] allColSpecs = new DataColumnSpec[4];
        allColSpecs[0] = 
            new DataColumnSpecCreator("GS", ListCell.getCollectionType(DoubleCell.TYPE)).createSpec();
        allColSpecs[1] = 
            new DataColumnSpecCreator("Label", StringCell.TYPE).createSpec();
        allColSpecs[2] = 
            new DataColumnSpecCreator("Width", IntCell.TYPE).createSpec();
        allColSpecs[3] = 
                new DataColumnSpecCreator("Height", IntCell.TYPE).createSpec();
        DataTableSpec outputSpec = new DataTableSpec(allColSpecs);
        
        
        BufferedDataContainer container = exec.createDataContainer(outputSpec);
        
       	for (DataRow row : inData[0]){
    	   String letter = ((StringCell) row.getCell(inData[0].getSpec().findColumnIndex("letter"))).getStringValue();
    	   int width = ((IntCell) row.getCell(inData[0].getSpec().findColumnIndex("Width"))).getIntValue();
    	   int height = ((IntCell) row.getCell(inData[0].getSpec().findColumnIndex("Height"))).getIntValue();
    	   
    	   if (!letters.containsKey(letter))
    	   {
    		   letters.put(letter, new Letter(letter, width, height, new float[width*height]));
    		   counts.put(letter, 1);
    	   }
    	   else
    	   {
    		   counts.put(letter, counts.get(letter) + 1);
    	   }
    	   
    	   Letter l = letters.get(letter);
    	   
    	   ListCell cel = (ListCell) row.getCell(inData[0].getSpec().findColumnIndex("GS"));
    	   for(int i = 0; i < l.data.length; ++ i)
    			  l.data[i] += ((IntCell)cel.get(i)).getDoubleValue();
    	   
       	}
       	
       	int id = 0;
        for(Entry<String, Letter> entry : letters.entrySet()){ // erh�hen falls die X kommen
        	Letter l = entry.getValue();
        	int count = counts.get(entry.getKey());
     		for (int i = 0; i < l.data.length; i++)
     			l.data[i] /= count;
     		
     		cropByTreshold(l, (float)TRESHOLD_SETTING.getDoubleValue());
        	
        	RowKey key = new RowKey("Row" + ++id);
        	DataCell[] cell = new DataCell[4];
        	List<DoubleCell> doubleCells = new ArrayList<>(l.width * l.height);	
        	for(int i = 0; i<l.data.length; i++){
        		doubleCells.add(new DoubleCell(l.data[i]));
        	}
        	cell[0] = CollectionCellFactory.createListCell(doubleCells);
        	cell[1] = new StringCell(entry.getKey());
        	cell[2] = new IntCell(l.width);
        	cell[3] = new IntCell(l.height);
        	DataRow row = new DefaultRow(key, cell);
        	container.addRowToTable(row);
        }
            
        
        // once we are done, we close the container and return its table
        container.close();
        BufferedDataTable out = container.getTable();
        return new BufferedDataTable[]{out};
    }
    
    public void cropByTreshold(Letter letter, float threshold)
    {
    	int maxX = 0, minX = letter.width - 1;
    	int maxY = 0, minY = letter.height - 1;

    	for (int y = 0; y < letter.height; y++)
    	{
	    	for (int x = 0; x < letter.width; x++)
	    	{
	    		float pixel = letter.data[x*letter.height+y];
	    		
        		if (pixel < threshold)
        		{
        			if (maxX < x) maxX = x;
        			if (minX > x) minX = x;
        			if (maxY < y) maxY = y;
        			if (minY > y) minY = y;
        		}
        	}
    	}
    	
    	int newWidth = maxX - minX + 1;
    	int newHeight = maxY - minY + 1;
    	
    	float[] data = new float[newWidth*newHeight];
    	
    	int i = 0;
	    for (int x = minX; x <= maxX; x++)
	    {
	        for (int y = minY; y <= maxY; y++)
	        {
	    		data[i++] = letter.data[x*letter.height+y];
	    	}
    	}
    	
    	
    	letter.width = newWidth;
    	letter.height = newHeight;
    	letter.data = data;    	
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void reset() {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected DataTableSpec[] configure(final DataTableSpec[] inSpecs)
            throws InvalidSettingsException {
        return new DataTableSpec[]{null};
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void saveSettingsTo(final NodeSettingsWO settings) {
    	TRESHOLD_SETTING.saveSettingsTo(settings);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void loadValidatedSettingsFrom(final NodeSettingsRO settings)
            throws InvalidSettingsException {
    	TRESHOLD_SETTING.loadSettingsFrom(settings);

    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void validateSettings(final NodeSettingsRO settings)
            throws InvalidSettingsException {
    	TRESHOLD_SETTING.validateSettings(settings);
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    protected void loadInternals(final File internDir,
            final ExecutionMonitor exec) throws IOException,
            CanceledExecutionException {
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    protected void saveInternals(final File internDir,
            final ExecutionMonitor exec) throws IOException,
            CanceledExecutionException {
    }

}

