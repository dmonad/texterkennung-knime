package org.knimelab.ocr.extractor;

import org.knime.core.data.DataTableSpec;
import org.knime.core.node.InvalidSettingsException;
import org.knime.core.node.NodeSettingsRO;
import org.knime.core.node.NotConfigurableException;
import org.knime.core.node.defaultnodesettings.DefaultNodeSettingsPane;
import org.knime.core.node.defaultnodesettings.DialogComponentBoolean;
import org.knime.core.node.defaultnodesettings.DialogComponentNumber;

/**
 * <code>NodeDialog</code> for the "FloodFillExtractor" Node.
 *
 * This node dialog derives from {@link DefaultNodeSettingsPane} which allows
 * creation of a simple dialog with standard components. If you need a more 
 * complex dialog please derive directly from 
 * {@link org.knime.core.node.NodeDialogPane}.
 * 
 * @author Team kNN
 */
public class FloodFillExtractorNodeDialog extends DefaultNodeSettingsPane
{
	final FloodFillExtractorNodeSettings settings = new FloodFillExtractorNodeSettings();	
	
    /**
     * New pane for configuring the FloodFillExtractor node.
     */
    protected FloodFillExtractorNodeDialog()
    {
    	super();
    	
    	addDialogComponent(new DialogComponentNumber(settings.threshold, "Threshold:", 10));
    	addDialogComponent(new DialogComponentNumber(settings.minWidth, "Minimal width:", 1));
    	addDialogComponent(new DialogComponentNumber(settings.minHeight, "Minimal height:", 1));
    	addDialogComponent(new DialogComponentBoolean(settings.path, "Include paths"));
    	addDialogComponent(new DialogComponentBoolean(settings.normRotation, "Normalize rotation"));
    	addDialogComponent(new DialogComponentBoolean(settings.heightWidthAdd, "Attach height/width-proportion"));
    }
    
    @Override
    protected void loadSettingsFrom(final NodeSettingsRO settings,
            final DataTableSpec[] specs) throws NotConfigurableException
    {
        try
        {
            this.settings.loadSettings(settings);
        }
        catch (InvalidSettingsException ex)
        {
            // Ignore errors and use the defaults
        }
    }
}

