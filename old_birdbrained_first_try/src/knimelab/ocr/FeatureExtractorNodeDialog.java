package knimelab.ocr; 

import org.knime.core.node.defaultnodesettings.DefaultNodeSettingsPane;
import org.knime.core.node.defaultnodesettings.DialogComponentNumber;
import org.knime.core.node.defaultnodesettings.SettingsModelIntegerBounded;

/**
 * <code>NodeDialog</code> for the "FeatureExtractor" Node.
 * 
 *
 * This node dialog derives from {@link DefaultNodeSettingsPane} which allows
 * creation of a simple dialog with standard components. If you need a more 
 * complex dialog please derive directly from 
 * {@link org.knime.core.node.NodeDialogPane}.
 * 
 * @author KNN
 */
public class FeatureExtractorNodeDialog extends DefaultNodeSettingsPane {

    /**
     * New pane for configuring FeatureExtractor node dialog.
     * This is just a suggestion to demonstrate possible default dialog
     * components.
     */
    protected FeatureExtractorNodeDialog() {
        super();                    
    }
}

