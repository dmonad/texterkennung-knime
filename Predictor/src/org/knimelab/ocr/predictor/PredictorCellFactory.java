package org.knimelab.ocr.predictor;
 
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;

import javax.imageio.ImageIO;

import org.knime.core.data.DataCell;
import org.knime.core.data.DataColumnSpec;
import org.knime.core.data.DataRow;
import org.knime.core.data.DataType;
import org.knime.core.data.collection.ListCell;
import org.knime.core.data.container.AbstractCellFactory;
import org.knime.core.data.def.DoubleCell;
import org.knime.core.data.def.IntCell;
import org.knime.core.data.def.StringCell;
import org.knime.core.data.image.png.PNGImageContent;
import org.knime.core.data.image.png.PNGImageValue;
import org.knime.core.node.BufferedDataTable;
import org.knime.core.node.ExecutionContext;
import org.knime.core.node.NodeLogger;

public class PredictorCellFactory extends AbstractCellFactory {

	BufferedDataTable[] inData;
	PredictorNodeSettings settings;

	int imageLearnedIndex, imageDataIndex = 0, letterNameIndex = 0;
	double numOfDataRows = 0;


	final ExecutionContext exec;

	private static int tmp_enumerator_count = 0;
	private int tmp_enumerator;

	synchronized void setTmpEnumerator() {
		tmp_enumerator = tmp_enumerator_count++;
	}

	/* Sorry, this is a lit overblown constructor */
	protected PredictorCellFactory(final ExecutionContext exec,
			PredictorNodeSettings settings, BufferedDataTable[] inData,
			boolean concurrently, DataColumnSpec[] spec) {

		super(concurrently, spec);

		setTmpEnumerator();

		this.exec = exec;

		this.inData = inData;
		this.settings = settings;
		if(inData != null){
			imageLearnedIndex = inData[0].getDataTableSpec().findColumnIndex(
					settings.getLearnerImageName());
			imageDataIndex = inData[1].getDataTableSpec().findColumnIndex(
					settings.getDataImageName());
			letterNameIndex = inData[0].getDataTableSpec().findColumnIndex(
					settings.getLearnerImageLetter());
	
		}
	}

	@Override
	public DataCell[] getCells(DataRow dataRow) {
		BufferedImage imageData, imageLearned;

		Object oL = null, oD = dataRow.getCell(imageDataIndex);
		BufferedImage oLscaled = null, oDscaled = null;
		imageData = (BufferedImage) ((PNGImageValue) oD).getImageContent()
				.getImage();

		double similarity = 0;
		String bestLetter = "Error";
		int width_scaleTo = 0, height_scaleTo = 0, width1 = 0, height1 = 0, width2 = 0, height2 = 0;

		try {
			int[] dataIndexesProjection = new int[settings.maxProjectionModels];
			int[] learningIndexesProjection = new int[settings.maxProjectionModels];
			
			// get Indexes of cols for projection function 
			for(int i=0; i<settings.maxProjectionModels; i++){
				if(!settings.addProjectionButtonModel[i].getBooleanValue()) 
					break;
				String dataStr = settings.addProjectionDataModel[i].getStringValue();
 				String learnStr = settings.addProjectionLearningModel[i].getStringValue();
				dataIndexesProjection[i] = inData[1].getSpec().findColumnIndex(dataStr);
				learningIndexesProjection[i] = inData[0].getSpec().findColumnIndex(learnStr);
			}
			double checkUntil = settings.takeAtLeastRelative.getDoubleValue()*inData[0].getRowCount();
			if(!settings.addProjectionButtonModel[0].getBooleanValue() || checkUntil <1){
				checkUntil = inData[0].getRowCount();
			}	

			for(int checkedObjects = 0, iteration = 1
					; (checkedObjects < checkUntil || similarity < settings.minimumPropability.getDoubleValue())
					  && iteration < 100
					; iteration++ ) {
				
				for (DataRow learnRow : inData[0]) {
					
					// check if it is in projection range
					if(settings.addProjectionButtonModel[0].getBooleanValue() ){
						boolean specialCase = false;
						boolean continueNow = false;
						for(int i=0; i<settings.maxProjectionModels
								&& settings.addProjectionButtonModel[i].getBooleanValue() 
								; i++){
							DataCell Oa = learnRow.getCell(learningIndexesProjection[i]);
							DataCell Ob =  dataRow.getCell(dataIndexesProjection[i]);
							double a,b;
							double distance = 0;
							if(Oa instanceof ListCell){
								 if(((ListCell) Oa).getElementType() == DoubleCell.TYPE){
									 	distance = 0;
										ListCell listA = (ListCell) Oa;
										ListCell listB = (ListCell) Ob;
										for(int e=Math.min(listA.size(), listB.size()) -1; 
												e>=0;
												e--){
												double dis =  
														((Double)((DoubleCell)listA.get(e)).getDoubleValue())
															- ((Double)((DoubleCell)listB.get(e)).getDoubleValue()) ;
												distance += dis*dis;
										}
										distance = Math.sqrt(distance);
								 }
							} else {
								if(Oa.getType() == DoubleCell.TYPE){
									a = ((DoubleCell) Oa).getDoubleValue();
								} else {
									a = ((IntCell) Oa).getIntValue();
								}
								if(Ob instanceof DoubleCell){
									b = ((DoubleCell) Ob).getDoubleValue();
								} else {
									b = ((IntCell) Ob).getIntValue();
								}
								distance = Math.abs(a -b);
							}
							if(!(distance <= settings.distanceStepModel[i].getDoubleValue()*iteration)){
								continueNow = true;
								break;
							}
							if(distance > settings.distanceStepModel[i].getDoubleValue()*(iteration-1))
								specialCase = true;
						}
						if(continueNow || !specialCase) continue;
					}
					// now check this Object
					checkedObjects++;
					
					oL = learnRow.getCell(imageLearnedIndex);
					if (oL == null)
						continue;
					imageLearned = (BufferedImage) ((PNGImageValue) oL)
							.getImageContent().getImage();
					if (imageLearned == null)
						continue;

					// scale images
					if (settings.scaleUp()) {
						width_scaleTo = Math.max(imageLearned.getWidth(),
								imageData.getWidth());
						height_scaleTo = Math.max(imageLearned.getHeight(),
								imageData.getHeight());
					} else {
						width_scaleTo = Math.min(imageLearned.getWidth(),
								imageData.getWidth());
						height_scaleTo = Math.min(imageLearned.getHeight(),
								imageData.getHeight());
					}
					if (settings.scaleImagesByHeight()) {
						height1 = height2 = height_scaleTo;
					} else {
						height1 = imageLearned.getHeight();
						height2 = imageData.getHeight();
					}
					if (settings.scaleImagesByWidth()) {
						width1 = width2 = width_scaleTo;
						if (settings.scaleImagesByHeight() == false) { // relatively
																		// scale
							double scalefactor1 = (width1 * height_scaleTo)
									/ width_scaleTo;
							height1 = (int) scalefactor1;
							double scalefactor2 = (width2 * height_scaleTo)
									/ width_scaleTo;
							height2 = (int) scalefactor2;
						}
					} else {
						width1 = imageLearned.getWidth();
						width2 = imageData.getWidth();
						if (settings.scaleImagesByHeight()) { // relatively
																// scale
							double scalefactor1 = (height1 * width_scaleTo)
									/ height_scaleTo;
							width1 = (int) scalefactor1;
							double scalefactor2 = (height2 * width_scaleTo)
									/ height_scaleTo;
							width2 = (int) scalefactor2;
						}
					}
					// Now we cut the bigger image to the size of the smaller
					// one
					int widthMin = Math.min(width1, width2), heightMin = Math
							.min(height1, height2);
					int x1 = (width1 - widthMin) / 2, y1 = (height1 - heightMin) / 2, x2 = (width2 - widthMin) / 2, y2 = (height2 - heightMin) / 2;

					int scaleFunction = settings.getScaleFunction();
					Image i1 = imageLearned.getScaledInstance(width1, height1,
							scaleFunction);
					Image i2 = imageData.getScaledInstance(width2, height2,
							scaleFunction);

					// get the buffered Image - As far as I know, this is the
					// only working way
					BufferedImage image1 = new BufferedImage(width1, height1,
							imageLearned.getType()).getSubimage(x1, y1,
							widthMin, heightMin);
					BufferedImage image2 = new BufferedImage(width2, height2,
							imageData.getType()).getSubimage(x2, y2, widthMin,
							heightMin);
					Graphics2D g1 = image1.createGraphics();
					Graphics2D g2 = image2.createGraphics();
					g1.drawImage(i1, null, null);
					g2.drawImage(i2, null, null);
					g1.dispose();
					g2.dispose();

					// calculate similarity
					double similarity_ = calcSimilarity(image1, image2,
							widthMin, heightMin);

					if (similarity < similarity_) {
						similarity = similarity_;
						bestLetter = ((StringCell) learnRow
								.getCell(letterNameIndex)).getStringValue();
						oLscaled = image1;
						oDscaled = image2;
					}
				}
			}
			DataCell[] cells = null;

			if (settings.giveBackScaledImages()) {
				File tmpL = File.createTempFile(tmp_enumerator
						+ "knime_predictor_tmpL" + dataRow.getKey(), ".png");
				ImageIO.write(oLscaled, "png", tmpL);
				File tmpD = File.createTempFile(tmp_enumerator
						+ "knime_predictor_tmpD" + dataRow.getKey(), ".png");
				ImageIO.write(oDscaled, "png", tmpD);

				cells = new DataCell[] {
						(new PNGImageContent(new FileInputStream(tmpD)))
								.toImageCell(),
						(new PNGImageContent(new FileInputStream(tmpL)))
								.toImageCell(), new StringCell(bestLetter),
						new DoubleCell(similarity) };
			} else {
				cells = new DataCell[] { new StringCell(bestLetter),
						new DoubleCell(similarity) };
			}
			return cells;
		} catch (Exception e) {
			NodeLogger.getLogger(PredictorCellFactory.class).error("Error in CellFactory: " + e.getMessage());
			DataCell[] cells;
			if (settings.giveBackScaledImages()) {
				
				cells = new DataCell[] {
						DataType.getMissingCell(),
						DataType.getMissingCell(),
						DataType.getMissingCell(),
						DataType.getMissingCell()};
			} else {
				cells = new DataCell[] { 
						DataType.getMissingCell(),
						DataType.getMissingCell()
				};
			}
			return cells;
		}
	}

	private double calcSimilarity(BufferedImage image1, BufferedImage image2,
			int widthMin, int heightMin) throws Exception {
		double similarity_ = 0;
		if (settings.isCurrentSimilarityFunction("Cosine similarity (to black)")) {
			// Cosine similarity
			double sumA = 1, sumB = 1, sum = 1;
			for (int x = 0; x < widthMin; x++) {
				for (int y = 0; y < heightMin; y++) {
					int a = 255 - (new Color(image1.getRGB(x, y)).getBlue());
					int b = 255 - (new Color(image2.getRGB(x, y)).getBlue());

					sumA += a * a;
					sumB += b * b;
					sum += a * b;
				}
			}
			similarity_ = 1 - (2 * Math.acos(sum
					/ (Math.sqrt(sumA) * Math.sqrt(sumB))) / Math.PI);
		} else if (settings.isCurrentSimilarityFunction("Cosine similarity (to white)")) {
			// Cosine similarity
			double sumA = 1, sumB = 1, sum = 1;
			for (int x = 0; x < widthMin; x++) {
				for (int y = 0; y < heightMin; y++) {
					int a = (new Color(image1.getRGB(x, y)).getBlue());
					int b = (new Color(image2.getRGB(x, y)).getBlue());

					sumA += a * a;
					sumB += b * b;
					sum += a * b;
				}
			}
			similarity_ = 1 - (2 * Math.acos(sum
					/ (Math.sqrt(sumA) * Math.sqrt(sumB))) / Math.PI);

		} else if (settings.isCurrentSimilarityFunction("p=2 Distance function")) {
			// p=2 distance function
			for (int x = 0; x < widthMin; x++) {
				for (int y = 0; y < heightMin; y++) {
					int diff = (new Color(image1.getRGB(x, y)).getBlue())
							- (new Color(image2.getRGB(x, y)).getBlue());
					similarity_ += diff * diff;
				}
			}
			similarity_ = 1 - Math.sqrt(similarity_)
					/ (widthMin * heightMin * 255);
		} else {
			throw new Exception("Don't know similarity function: "
					+ settings.getSimilarityFunction());
		}
		return similarity_;
	}

}