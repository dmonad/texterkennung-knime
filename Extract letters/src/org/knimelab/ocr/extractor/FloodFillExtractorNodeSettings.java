package org.knimelab.ocr.extractor;

import org.knime.core.node.InvalidSettingsException;
import org.knime.core.node.NodeSettingsRO;
import org.knime.core.node.NodeSettingsWO;
import org.knime.core.node.defaultnodesettings.SettingsModelBoolean;
import org.knime.core.node.defaultnodesettings.SettingsModelInteger;

public class FloodFillExtractorNodeSettings
{
	final SettingsModelInteger threshold = new SettingsModelInteger("Treshold", 150);
	final SettingsModelInteger minWidth = new SettingsModelInteger("MinWidth", 3);
	final SettingsModelInteger minHeight = new SettingsModelInteger("MinHeight", 3);
	final SettingsModelBoolean path = new SettingsModelBoolean("Path", false);
    final SettingsModelBoolean normRotation = new SettingsModelBoolean("Normalize Rotation", true);
    final SettingsModelBoolean heightWidthAdd = new SettingsModelBoolean("Attach Height/Width-proportion", false);

    
    public boolean isAddHeightWidthProportion(){
    	return heightWidthAdd.getBooleanValue();
    }
	 
	public int getThreshold()
	{
		return threshold.getIntValue();
	}
	
	public int getMinWidth()
	{
		return minWidth.getIntValue();
	}
	
	public int getMinHeight()
	{
		return minHeight.getIntValue();
	}

	public boolean normRotation()
	{
		return normRotation.getBooleanValue();
	}

	public boolean getPath() {
		return path.getBooleanValue();
	}

    public void saveSettings(final NodeSettingsWO settings)
    {
    	threshold.saveSettingsTo(settings);
    	minWidth.saveSettingsTo(settings);
    	minHeight.saveSettingsTo(settings);
    	path.saveSettingsTo(settings);
    	normRotation.saveSettingsTo(settings);
    	heightWidthAdd.saveSettingsTo(settings);
    }

    public void loadSettings(final NodeSettingsRO settings)
           throws InvalidSettingsException
    {
   		threshold.loadSettingsFrom(settings);
    	minWidth.loadSettingsFrom(settings);
    	minHeight.loadSettingsFrom(settings);
    	path.loadSettingsFrom(settings);
    	normRotation.loadSettingsFrom(settings);
    	heightWidthAdd.loadSettingsFrom(settings);
    }

    public void validateSettings(final NodeSettingsRO settings)
           throws InvalidSettingsException
    {
   		threshold.validateSettings(settings);
    	minWidth.validateSettings(settings);
    	minHeight.validateSettings(settings);
    	path.validateSettings(settings);
    	normRotation.validateSettings(settings);
    	heightWidthAdd.validateSettings(settings);
    }
}
