package knimelab.ocr;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

import org.knime.core.data.DataCell;
import org.knime.core.data.DataColumnSpec;
import org.knime.core.data.DataColumnSpecCreator;
import org.knime.core.data.DataRow;
import org.knime.core.data.DataTableSpec;
import org.knime.core.data.RowKey;
import org.knime.core.data.collection.CollectionCellFactory;
import org.knime.core.data.collection.ListCell;
import org.knime.core.data.container.CellFactory;
import org.knime.core.data.container.ColumnRearranger;
import org.knime.core.data.def.BooleanCell;
import org.knime.core.data.def.IntCell;
import org.knime.core.data.image.png.PNGImageBlobCell;
import org.knime.core.data.image.png.*;
import org.knime.core.data.image.png.PNGImageContent;
import org.knime.core.data.image.png.PNGImageCell;
import org.knime.core.node.BufferedDataTable;
import org.knime.core.node.CanceledExecutionException;
import org.knime.core.node.ExecutionContext;
import org.knime.core.node.ExecutionMonitor;
import org.knime.core.node.InvalidSettingsException;
import org.knime.core.node.NodeModel;
import org.knime.core.node.NodeSettingsRO;
import org.knime.core.node.NodeSettingsWO;


/**
 * This is the model implementation of FeatureExtractor.
 * 
 *
 * @author KNN
 */
public class FeatureExtractorNodeModel extends NodeModel
{
    protected FeatureExtractorNodeModel()
    {
        super(1, 1);
    }
    
	byte[][] pixels;
	int width, height;

    @Override
    protected BufferedDataTable[] execute(final BufferedDataTable[] inData,
            final ExecutionContext exec) throws Exception
    {    	
    	ColumnRearranger ca = new ColumnRearranger(inData[0].getSpec());
    	
    	ca.append(new CellFactory() {			
			@Override
			public void setProgress(int curRowNr, int rowCount, RowKey lastKey,
					ExecutionMonitor exec) {
				exec.setProgress(1.0 * curRowNr / rowCount);
			}
			
			@Override
			public DataColumnSpec[] getColumnSpecs() {
				return new DataColumnSpec[] {
						new DataColumnSpecCreator("GS", ListCell.getCollectionType(IntCell.TYPE)).createSpec(),
						new DataColumnSpecCreator("SW", ListCell.getCollectionType(BooleanCell.TYPE)).createSpec(),
						new DataColumnSpecCreator("Width", IntCell.TYPE).createSpec(),
						new DataColumnSpecCreator("Height", IntCell.TYPE).createSpec(),
						new DataColumnSpecCreator("LetterWidth", IntCell.TYPE).createSpec(),
						new DataColumnSpecCreator("LetterHeight", IntCell.TYPE).createSpec()
				};
			}
			
			@Override
			public DataCell[] getCells(DataRow row)  {
				PNGImageContent image = ((PNGImageValue)row.getCell(0)).getImageContent();		    	BufferedImage data;
				try {
					data = ImageIO.read(new ByteArrayInputStream(image.getByteArrayReference()));

			    	int[] pixels = data.getRGB(0, 0, data.getWidth(), data.getHeight(), null, 0, data.getWidth());
					
					List<IntCell> intCells = new ArrayList<>(pixels.length);
					List<BooleanCell> boolCells = new ArrayList<>(pixels.length);

					int minHeight = data.getHeight(), maxHeight = 0, minWidth = 0, maxWidth = 0;

					for (int x = 0; x < data.getWidth(); x++) {
						for (int y = 0; y < data.getHeight(); y++) {
							int value = new Color(data.getRGB(x, y)).getBlue();
							
							intCells.add(new IntCell(value));
							boolCells.add(BooleanCell.get(value < 255));
							
							if (minHeight > y && value != 255){
								minHeight = y;
							}
							if (maxHeight < y && value != 255){
								maxHeight = y;
							}
							
							if (minWidth == 0 && value != 255){
								minWidth = x;
							}
							if (maxWidth < x && value != 255){
								maxWidth = x;
							}
						}
					}
					
					int height = maxHeight - minHeight + 1;
					int width = maxWidth - minWidth + 1;
					
					return new DataCell[] {
							CollectionCellFactory.createListCell(intCells),
							CollectionCellFactory.createListCell(boolCells),
							new IntCell(data.getWidth()),
							new IntCell(data.getHeight()),
							new IntCell(width),
							new IntCell(height)
					};
				} catch (IOException e) {
				}
				
				return new DataCell[] {
						CollectionCellFactory.createListCell(new ArrayList<IntCell>()),
						CollectionCellFactory.createListCell(new ArrayList<BooleanCell>()),
						new IntCell(-1),
						new IntCell(-1),
						new IntCell(-1),
						new IntCell(-1)
				};
			}
		});
    	
    	BufferedDataTable out = exec.createColumnRearrangeTable(inData[0], ca, exec);
    	
    	return new BufferedDataTable[]{out};
    }

    @Override
	protected void reset() {
    }

    @Override
    protected DataTableSpec[] configure(final DataTableSpec[] inSpecs)
            throws InvalidSettingsException {

        return new DataTableSpec[]{null};
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void saveSettingsTo(final NodeSettingsWO settings) {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void loadValidatedSettingsFrom(final NodeSettingsRO settings)
            throws InvalidSettingsException {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void validateSettings(final NodeSettingsRO settings)
            throws InvalidSettingsException {
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    protected void loadInternals(final File internDir,
            final ExecutionMonitor exec) throws IOException,
            CanceledExecutionException {
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    protected void saveInternals(final File internDir,
            final ExecutionMonitor exec) throws IOException,
            CanceledExecutionException {
    }

}

