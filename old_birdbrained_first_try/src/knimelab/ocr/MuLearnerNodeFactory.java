package knimelab.ocr;

import org.knime.core.node.NodeDialogPane;
import org.knime.core.node.NodeFactory;
import org.knime.core.node.NodeView;

/**
 * <code>NodeFactory</code> for the "MuLearner" Node.
 * 
 *
 * @author KNN
 */
public class MuLearnerNodeFactory 
        extends NodeFactory<MuLearnerNodeModel> {

    /**
     * {@inheritDoc}
     */
    @Override
    public MuLearnerNodeModel createNodeModel() {
        return new MuLearnerNodeModel();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getNrNodeViews() {
        return 0;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public NodeView<MuLearnerNodeModel> createNodeView(final int viewIndex,
            final MuLearnerNodeModel nodeModel) {
    	return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasDialog() {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public NodeDialogPane createNodeDialogPane() {
        return new MuLearnerNodeDialog();
    }

}

