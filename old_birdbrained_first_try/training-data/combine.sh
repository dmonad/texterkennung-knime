#letters="ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"
letters="ABCDEFGHIJKLMNOPQRSTUVWXYZ"

OIFS="${IFS}"
NIFS=$'\n'
IFS="${NIFS}"

for (( i=0; i<${#letters}; i++ )); do
    letter=${letters:$i:1}
    files=$(find letters -iname "$letter-*.png")
    f=$(echo "$files" | head -n 1)
    w=$(identify -format "%[fx:w]" "$f")
    h=$(identify -format "%[fx:h]" "$f")
    s=$(printf "%sx%s" "$w" "$h")
    f="letters/$letter.png"
    fc=$(echo "$files" | wc -l)
    p=$(( 100 / $fc ))
    echo "$letter : $s"
    convert -background black -size "$s" label:" " -channel Black "$f"
    for file in $files
    do
        composite -blend $p "$file" -size "$s" xc:white -alpha Set "$file-t.png"
        mogrify -negate "$file-t.png"
        composite -compose Plus -gravity center "$file-t.png" "$f" "$f"
        rm "$file-t.png"
    done
    mogrify -negate "$f"
done
