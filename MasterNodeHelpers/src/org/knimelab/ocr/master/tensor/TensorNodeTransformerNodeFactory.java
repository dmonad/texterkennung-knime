package org.knimelab.ocr.master.tensor;

import org.knime.core.node.NodeDialogPane;
import org.knime.core.node.NodeFactory;
import org.knime.core.node.NodeView;

/**
 * <code>NodeFactory</code> for the "TensorNodeTransformer" Node.
 * 
 *
 * @author KNN
 */
public class TensorNodeTransformerNodeFactory 
        extends NodeFactory<TensorNodeTransformerNodeModel> {

    /**
     * {@inheritDoc}
     */
    @Override
    public TensorNodeTransformerNodeModel createNodeModel() {
        return new TensorNodeTransformerNodeModel();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getNrNodeViews() {
        return 1;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public NodeView<TensorNodeTransformerNodeModel> createNodeView(final int viewIndex,
            final TensorNodeTransformerNodeModel nodeModel) {
        return new TensorNodeTransformerNodeView(nodeModel);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasDialog() {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public NodeDialogPane createNodeDialogPane() {
        return new TensorNodeTransformerNodeDialog();
    }

}

