package knimelab.ocr;

import org.knime.core.node.NodeDialogPane;
import org.knime.core.node.NodeFactory;
import org.knime.core.node.NodeView;

/**
 * <code>NodeFactory</code> for the "PixelEnumerator" Node.
 * 
 *
 * @author 
 */
public class PixelEnumeratorNodeFactory 
        extends NodeFactory<PixelEnumeratorNodeModel> {

    /**
     * {@inheritDoc}
     */
    @Override
    public PixelEnumeratorNodeModel createNodeModel() {
        return new PixelEnumeratorNodeModel();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getNrNodeViews() {
        return 0;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public NodeView<PixelEnumeratorNodeModel> createNodeView(final int viewIndex,
            final PixelEnumeratorNodeModel nodeModel) {
    	return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasDialog() {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public NodeDialogPane createNodeDialogPane() {
        return new PixelEnumeratorNodeDialog();
    }

}

