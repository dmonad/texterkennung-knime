#letters="ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"
letters="ABCDEFGHIJKLMNOPQRSTUVWXYZ"

OIFS="${IFS}"
NIFS=$'\n'
IFS="${NIFS}"

for (( i=0; i<${#letters}; i++ )); do
    letter=${letters:$i:1}
    width=0
    height=0
    for file in $(find letters -iname "$letter-*.png")
    do
        w=$(identify -format "%[fx:w]" "$file")
        if [ "$w" -gt "$width" ]
        then
            width="$w"
        fi

        h=$(identify -format "%[fx:h]" "$file")
        if [ "$h" -gt "$height" ]
        then
            height="$h"
        fi
    done

    size=$(printf "%sx%s" "$width" "$height")

    for file in $(find letters -iname "$letter-*.png")
    do
        mogrify -gravity center -extent "$size" "$file"
    done

    printf "%s -> %sx%s\n" "$letter" "$width" "$height"
done
