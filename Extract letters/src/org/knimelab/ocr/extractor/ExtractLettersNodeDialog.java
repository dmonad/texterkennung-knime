package org.knimelab.ocr.extractor;

import org.knime.core.data.DataTableSpec;
import org.knime.core.node.InvalidSettingsException;
import org.knime.core.node.NodeSettingsRO;
import org.knime.core.node.NotConfigurableException;
import org.knime.core.node.defaultnodesettings.DefaultNodeSettingsPane;
import org.knime.core.node.defaultnodesettings.DialogComponentBoolean;
import org.knime.core.node.defaultnodesettings.DialogComponentNumberEdit;

/**
 * <code>NodeDialog</code> for the "ExtractLetters" Node.
 * This node extracts letters of a PNG-Image and gives back all the letters as PNG-Images
 *
 * This node dialog derives from {@link DefaultNodeSettingsPane} which allows
 * creation of a simple dialog with standard components. If you need a more 
 * complex dialog please derive directly from 
 * {@link org.knime.core.node.NodeDialogPane}.
 * 
 * @author Knime Lab Group
 */
public class ExtractLettersNodeDialog extends DefaultNodeSettingsPane {

	ExtractLettersNodeSettings settings = new ExtractLettersNodeSettings();
	
    /**
     * New pane for configuring ExtractLetters node dialog.
     * This is just a suggestion to demonstrate possible default dialog
     * components.
     */
    protected ExtractLettersNodeDialog() {
        super();           
        // threshold
        this.addDialogComponent(
        		new DialogComponentNumberEdit(settings.threshold, 
        				"Pixel threshold for detection (0 (Black) - 255 (White)"));
        
        this.addDialogComponent(
        		new DialogComponentBoolean(settings.extraInfo, "Attach some Information about the letter")
        		);

    }
    
    
    @Override
    protected void loadSettingsFrom(final NodeSettingsRO settings,
            final DataTableSpec[] specs) throws NotConfigurableException
    {
        try
        {
            this.settings.loadSettings(settings);
        }
        catch (InvalidSettingsException ex)
        {
            // Ignore errors and use the defaults
        }
    }
}

