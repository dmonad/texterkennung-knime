package knimelab.ocr;

import org.knime.core.node.defaultnodesettings.*;

/**
 * <code>NodeDialog</code> for the "MuLearner" Node.
 * 
 *
 * This node dialog derives from {@link DefaultNodeSettingsPane} which allows
 * creation of a simple dialog with standard components. If you need a more 
 * complex dialog please derive directly from 
 * {@link org.knime.core.node.NodeDialogPane}.
 * 
 * @author KNN
 */
public class MuLearnerNodeDialog extends DefaultNodeSettingsPane {

    /**
     * New pane for configuring MuLearner node dialog.
     * This is just a suggestion to demonstrate possible default dialog
     * components.
     */
    protected MuLearnerNodeDialog() {
        super();
        this.addDialogComponent(new DialogComponentNumber(MuLearnerNodeModel.TRESHOLD_SETTING, "Treshold", 1));
                    
    }
}

