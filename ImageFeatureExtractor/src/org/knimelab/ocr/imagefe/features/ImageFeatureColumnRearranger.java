package org.knimelab.ocr.imagefe.features;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.ParameterizedType;

import javax.imageio.ImageIO;

import org.knime.core.data.DataCell;
import org.knime.core.data.DataColumnSpec;
import org.knime.core.data.DataColumnSpecCreator;
import org.knime.core.data.DataRow;
import org.knime.core.data.DataTableSpec;
import org.knime.core.data.DataType;
import org.knime.core.data.container.AbstractCellFactory;
import org.knime.core.data.container.ColumnRearranger;
import org.knime.core.data.def.DoubleCell;
import org.knime.core.data.image.png.PNGImageCell;
import org.knime.core.data.image.png.PNGImageContent;
import org.knime.core.data.image.png.PNGImageValue;
import org.knimelab.ocr.imagefe.ImageFeatureExtractorNodeSettings;

abstract class ImageFeatureColumnRearranger<TData extends RowInfo> extends ColumnRearranger
{
	final DataColumnSpec[] specs;
	final int n;
	final int imageIndex;
	final boolean useBlackCenter;
	
	final ImageFeatureExtractorNodeSettings settings;
	
	public ImageFeatureColumnRearranger(DataTableSpec original,  ImageFeatureExtractorNodeSettings settings, String mode)
	{
		super(original);
		
		this.settings = settings;
    	
    	n = settings.getNumber();
    	specs = new DataColumnSpec[n+1];
    	imageIndex = original.findColumnIndex(settings.getImageColumn());
    	useBlackCenter = settings.isUsingBlackCenter();
    	
    	final Class<?> clazz = (Class<?>)((ParameterizedType)this.getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    	
    	specs[0] = new DataColumnSpecCreator("Debug image", DataType.getType(PNGImageCell.class)).createSpec();

    	for (int i = 1; i <= n; i++)
    	{
    		specs[i] = new DataColumnSpecCreator(mode + " " + i, DoubleCell.TYPE).createSpec();
    	}
    	
    	this.append(new AbstractCellFactory(true, specs)
    	{			
			@SuppressWarnings("unchecked")
			@Override
			public DataCell[] getCells(DataRow row)
			{
				double[] bins = new double[n];				
				BufferedImage image = (BufferedImage) ((PNGImageValue)row.getCell(imageIndex)).getImageContent().getImage();
				
				TData data;
				try
				{					
					data = (TData) clazz.newInstance();
				}
				catch (InstantiationException | IllegalAccessException e1)
				{
					return null;
				}

				data.width = image.getWidth();
				data.height = image.getHeight();
				
				if (useBlackCenter)
				{
					double[] center = findBlackCenter(image);
					
					data.cx = center[0];
					data.cy = center[1];
				}
				else
				{
					data.cx = data.width / 2d;
					data.cy = data.height / 2d;
				}
				
				data.hDiff = Math.max(data.cx, data.width - data.cx);
				data.vDiff = Math.max(data.cy, data.height - data.cy);
				
				setup(data);
				
				// Debug				
				BufferedImage debugImage = new BufferedImage(data.width, data.height, BufferedImage.TYPE_INT_ARGB);
				
				Graphics g = debugImage.createGraphics();
				
				g.setColor(Color.WHITE);				
				g.fillRect(0, 0, data.width, data.height);
				
				drawDebug(g, data);
				
				double t = 0;
				
		    	for (int x = 0; x < data.width; x++)
		    	{
		        	for (int y = 0; y < data.height; y++)
		        	{
		        		int pixel = new Color(image.getRGB(x, y)).getBlue();
		        		
		        		if (pixel < 255)
		        		{
			        		double f = (255 - pixel) / 255d;
			        		
			        		t += f;
			        		
			        		int bin = (int)Math.min(getBin(x, y, f, data), n - 1);
			        		
			        		bins[bin] += f;	
			        		
			        		int rC = pixel;
			        		int gC = pixel / 2 + (128 * bin / n);
			        		int bC = pixel / 2 + 128 - (128 * bin / n);

			        		g.setColor(new Color(rC, gC, bC));
			        		g.fillRect(x, y, 1, 1);	        			
		        		}		        	
		        	}
		    	}
				
				g.dispose();	
				
		    	DataCell[] cells = new DataCell[n+1];		    	

		        File outputfile;
				try
				{
					outputfile = File.createTempFile("knime_imagefe_debug_" + row.getKey(), ".png");
					ImageIO.write(debugImage, "png", outputfile);
		    	
		    		cells[0] = new PNGImageContent(new FileInputStream(outputfile)).toImageCell();
				}
				catch (IOException e)
				{
					return null;
				}
		    	
		    	for (int i = 1; i <= n; i++)
		    	{
		    		cells[i] = new DoubleCell(bins[i-1] / t);
		    	}
				
				return cells;
			}
		});
	}
    
    public static double[] findBlackCenter(BufferedImage image)
    {
    	double cx = 0, cy = 0;
    	int w = image.getWidth();
    	int h = image.getHeight();
    	double t = 0;
    	
    	for (int x = 0; x < w; x++)
    	{
        	for (int y = 0; y < h; y++)
        	{
        		int pixel = new Color(image.getRGB(x, y)).getBlue();
        		
        		if (pixel < 255)
        		{
	        		double f = (255 - pixel) / 255d;
	        		
	        		t += f;
	        		cx += f * x;
	        		cy += f * y;
        		}
        	}    		
    	}
    	
    	return new double[] {cx / t, cy / t};
    }
	
	abstract protected void setup(TData data);
	
	abstract protected void drawDebug(Graphics g, TData data);
	
	abstract protected int getBin(int x, int y, double f, TData data);
}