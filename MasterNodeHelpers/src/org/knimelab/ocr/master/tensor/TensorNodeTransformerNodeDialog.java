package org.knimelab.ocr.master.tensor;

import org.knime.core.node.defaultnodesettings.DefaultNodeSettingsPane;

/**
 * <code>NodeDialog</code> for the "TensorNodeTransformer" Node.
 * 
 *
 * This node dialog derives from {@link DefaultNodeSettingsPane} which allows
 * creation of a simple dialog with standard components. If you need a more 
 * complex dialog please derive directly from 
 * {@link org.knime.core.node.NodeDialogPane}.
 * 
 * @author KNN
 */
public class TensorNodeTransformerNodeDialog extends DefaultNodeSettingsPane {

    /**
     * New pane for configuring TensorNodeTransformer node dialog.
     * This is just a suggestion to demonstrate possible default dialog
     * components.
     */
    protected TensorNodeTransformerNodeDialog() {
        super();
        
        ;
                    
    }
}

