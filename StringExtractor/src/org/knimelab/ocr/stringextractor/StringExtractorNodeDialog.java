package org.knimelab.ocr.stringextractor;

import org.knime.core.node.defaultnodesettings.DefaultNodeSettingsPane;

/**
 * <code>NodeDialog</code> for the "StringExtractor" Node.
 * 
 *
 * This node dialog derives from {@link DefaultNodeSettingsPane} which allows
 * creation of a simple dialog with standard components. If you need a more 
 * complex dialog please derive directly from 
 * {@link org.knime.core.node.NodeDialogPane}.
 * 
 * @author Team kNN
 */
public class StringExtractorNodeDialog extends DefaultNodeSettingsPane {

    /**
     * New pane for configuring the StringExtractor node.
     */
    protected StringExtractorNodeDialog() {

    }
}

