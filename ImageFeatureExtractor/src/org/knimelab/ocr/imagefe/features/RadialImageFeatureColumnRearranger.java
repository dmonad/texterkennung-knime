package org.knimelab.ocr.imagefe.features;

import java.awt.Color;
import java.awt.Graphics;

import org.knime.core.data.DataTableSpec;
import org.knimelab.ocr.imagefe.ImageFeatureExtractorNodeSettings;

public class RadialImageFeatureColumnRearranger extends ImageFeatureColumnRearranger<RadialRowInfo>
{
	public RadialImageFeatureColumnRearranger(DataTableSpec original,
			ImageFeatureExtractorNodeSettings settings)
	{
		super(original, settings, "Radial");
	}

	@Override
	protected void setup(RadialRowInfo data)
	{
		double factor = data.hDiff / data.vDiff;
		
		data.ry = Math.sqrt(factor * factor * data.vDiff * data.vDiff + data.hDiff * data.hDiff) / factor;
		data.rx = data.ry * data.hDiff / data.vDiff;
		
		data.rx *= 0.9;
		data.ry *= 0.9;

		data.rx2 = data.rx * data.rx + 1;
		data.ry2 = data.ry * data.ry + 1;		
	}	
	
	@Override
	protected void drawDebug(Graphics g, RadialRowInfo data)
	{
		g.setColor(Color.RED);
		
		for (double f = 1; f > 0; f -= 1f/n)
		{
			g.drawOval((int)(data.cx - f*data.rx), (int)(data.cy - f*data.ry), (int)(2*f*data.rx), (int)(2*f*data.ry));
		}		
		
	}

	@Override
	protected int getBin(int x, int y, double f, RadialRowInfo data)
	{		
		double dx = data.cx - x;
		double dy = data.cy - y;
		
		double d = (dx * dx) / data.rx2 + (dy * dy) / data.ry2;
		
		return (int) (d * n);
	}
}