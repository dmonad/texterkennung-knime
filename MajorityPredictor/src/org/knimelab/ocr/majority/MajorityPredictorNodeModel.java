package org.knimelab.ocr.majority;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.knime.core.data.DataCell;
import org.knime.core.data.DataColumnSpec;
import org.knime.core.data.DataColumnSpecCreator;
import org.knime.core.data.DataRow;
import org.knime.core.data.DataTableSpec;
import org.knime.core.data.container.ColumnRearranger;
import org.knime.core.data.container.SingleCellFactory;
import org.knime.core.data.def.DoubleCell;
import org.knime.core.data.def.StringCell;
import org.knime.core.node.BufferedDataTable;
import org.knime.core.node.CanceledExecutionException;
import org.knime.core.node.ExecutionContext;
import org.knime.core.node.ExecutionMonitor;
import org.knime.core.node.InvalidSettingsException;
import org.knime.core.node.NodeModel;
import org.knime.core.node.NodeSettingsRO;
import org.knime.core.node.NodeSettingsWO;
import org.knime.core.util.MutableDouble;

/**
 * This is the model implementation of MajorityPredictor.
 * 
 *
 * @author Team kNN
 */
public class MajorityPredictorNodeModel extends NodeModel
{    
	protected final MajorityPredictorNodeSettings settings = new MajorityPredictorNodeSettings();
	
	protected static final HashMap<String, double[]> WEIGHTS = new HashMap<String, double[]>();

	static {
		WEIGHTS.put("Angular", new double[] {
				0.803030303030303,
				0.8852459016393444,
				0.7559055118110236,
				0.5985401459854014,
				0.7938931297709925,
				0.5789473684210525,
				0.9457364341085271,
				0.5871559633027522,
				0.5354330708661418,
				0.7804878048780488,
				0.7272727272727272,
				0.5441176470588236,
				0.6607142857142857,
				0.8188976377952757,
				0.6666666666666666,
				0.8095238095238095,
				0.7796610169491526,
				0.7288135593220338,
				0.8064516129032259,
				0.6942148760330578,
				0.7519999999999999,
				0.6306306306306306,
				0.8656716417910448,
				0.7796610169491526,
				0.7731092436974789,
				0.7179487179487181,
				0.6782608695652174,
				0.7230769230769231,
				0.7017543859649122,
				0.5470085470085471,
				0.671875,
				0.7462686567164178,
				0.8429752066115703,
				0.6942148760330578,
				0.7563025210084033,
				0.8943089430894309,
				0.7874015748031497,
				0.762589928057554
		});
		WEIGHTS.put("Radial", new double[] {
				0.803030303030303,
				0.2,
				0.7559055118110236,
				0.5985401459854014,
				0.7938931297709925,
				0.5789473684210525,
				0.9457364341085271,
				0.5871559633027522,
				0.5354330708661418,
				0.7804878048780488,
				0.7272727272727272,
				0.5441176470588236,
				0.6607142857142857,
				0.8188976377952757,
				0.6666666666666666,
				0.8095238095238095,
				0.7796610169491526,
				0.7288135593220338,
				0.8064516129032259,
				0.6942148760330578,
				0.7519999999999999,
				0.6306306306306306,
				0.8656716417910448,
				0.7796610169491526,
				0.7731092436974789,
				0.7179487179487181,
				0.6782608695652174,
				0.7230769230769231,
				0.7017543859649122,
				0.5470085470085471,
				0.671875,
				0.7462686567164178,
				0.8429752066115703,
				0.6942148760330578,
				0.7563025210084033,
				0.8943089430894309,
				0.7874015748031497,
				0.762589928057554
		});
		WEIGHTS.put("Horizontal", new double[] {
				0.8099173553719008,
				0.7172413793103448,
				0.6086956521739131,
				0.8333333333333333,
				0.8345323741007195,
				0.8787878787878788,
				0.8405797101449275,
				0.6481481481481481,
				0.7938931297709922,
				0.765217391304348,
				0.7328244274809159,
				0.768,
				0.6554621848739496,
				0.7874015748031497,
				0.8813559322033898,
				0.819672131147541,
				0.816,
				0.7894736842105263,
				0.7818181818181819,
				0.8709677419354839,
				0.7413793103448276,
				0.8571428571428571,
				0.8760330578512396,
				0.8717948717948717,
				0.882882882882883,
				0.7894736842105263,
				0.5655172413793104,
				0.9016393442622952,
				0.7307692307692308,
				0.8288288288288288,
				0.6929133858267716,
				0.8676470588235293,
				0.8032786885245902,
				0.6290322580645161,
				0.8495575221238939,
				0.7681159420289856,
				0.8484848484848485,
				0.7819548872180451,
		});
		WEIGHTS.put("Vertical", new double[] {
				0.8907563025210085,
				0.2,
				0.6493506493506495,
				0.912,
				0.9465648854961831,
				0.8527131782945736,
				0.9682539682539683,
				0.9677419354838709,
				0.9593495934959351,
				0.8702290076335878,
				0.7903225806451613,
				0.976,
				0.9586776859504132,
				0.6341463414634146,
				0.8793103448275862,
				0.64,
				0.9016393442622952,
				0.9833333333333333,
				0.9090909090909091,
				0.8214285714285715,
				0.7101449275362318,
				0.8672566371681416,
				0.8032786885245902,
				0.959349593495935,
				0.6434782608695653,
				0.6942148760330578,
				0.6034482758620691,
				0.9677419354838709,
				0.8264462809917356,
				0.8166666666666667,
				0.817391304347826,
				0.8376068376068376,
				0.8923076923076922,
				0.8099173553719008,
				0.7230769230769231,
				0.943089430894309,
				0.96,
				0.8617886178861789,
		});
		WEIGHTS.put("EHD", new double[] {0.3893805309734513,
				0.5088757396449705,
				0.6375000000000001,
				0.6614173228346457,
				0.588235294117647,
				0.638655462184874,
				0.8064516129032259,
				0.6774193548387096,
				0.46296296296296297,
				0.7288135593220338,
				0.5777777777777778,
				0.5645161290322581,
				0.5161290322580645,
				0.4642857142857142,
				0.33587786259541985,
				0.4492753623188406,
				0.36697247706422015,
				0.4,
				0.45901639344262296,
				0.562962962962963,
				0.37254901960784315,
				0.39999999999999997,
				0.7359999999999999,
				0.27941176470588236,
				0.5210084033613445,
				0.6942148760330578,
				0.42975206611570244,
				0.4695652173913043,
				0.4122137404580153,
				0.45217391304347826,
				0.5245901639344263,
				0.5079365079365079,
				0.3333333333333333,
				0.3969465648854962,
				0.4375,
				0.65,
				0.5299145299145298,
				0.7317073170731708,
		});
		WEIGHTS.put("Tensors", new double[] {
				0.9166666666666667,
				0.7826086956521738,
				0.8936170212765957,
				0.9448818897637796,
				0.9701492537313433,
				1.0,
				1.0,
				0.9612403100775193,
				0.96875,
				0.9516129032258064,
				0.8976377952755906,
				0.976,
				0.9917355371900827,
				0.8387096774193549,
				0.9586776859504132,
				0.9917355371900827,
				0.8760330578512396,
				0.8524590163934426,
				0.9026548672566372,
				0.7375886524822695,
				0.5486725663716814,
				0.9411764705882353,
				1.0,
				0.9032258064516128,
				0.875,
				0.9747899159663865,
				0.8907563025210085,
				1.0,
				0.9516129032258064,
				0.9107142857142857,
				0.9411764705882353,
				0.8376068376068376,
				0.9516129032258064,
				0.8983050847457625,
				0.8959999999999999,
				1.0,
				0.9572649572649572,
				0.959349593495935,
		});
		WEIGHTS.put("Predictor", new double[] {
				2.5,
				2.5,
				2.5,
				2.5,
				2.5,
				2.5,
				2.5,
				2.5,
				2.5,
				2.5,
				2.5,
				2.5,
				2.5,
				2.5,
				2.5,
				2.5,
				2.5,
				2.5,
				2.5,
				2.5,
				2.5,
				2.5,
				2.5,
				2.5,
				2.5,
				2.5,
				2.5,
				2.5,
				2.5,
				2.5,
				2.5,
				2.5,
				2.5,
				2.5,
				2.5,
				2.5,
				2.5,
				2.5,
		});
	}
	
	protected int getIndex(String letter)
	{
		if (letter.charAt(0) == ',') return 0;
		if (letter.charAt(0) == '.') return 1;
		if (letter.charAt(0) >= '0' && letter.charAt(0) <= '9') return 2 + letter.charAt(0) - '0';
		
		return letter.charAt(0) - 'A' + 12;
	}
	
    /**
     * Constructor for the node model.
     */
    protected MajorityPredictorNodeModel()
    {
        super(1, 1);
    }
    
    protected ColumnRearranger getColumnRearranger(final DataTableSpec inSpec, final int[] indices, final int[] probIndices, final String[] types)
    {
		ColumnRearranger columnRearranger = new ColumnRearranger(inSpec);
		
		final DataColumnSpec spec = new DataColumnSpecCreator("Majority", StringCell.TYPE).createSpec();
		
		columnRearranger.append(new SingleCellFactory(true, spec)
		{			
			@Override
			public DataCell getCell(DataRow row)
			{
				HashMap<String, MutableDouble> scores = new HashMap<String, MutableDouble>();
				HashMap<String, String> prediction = new HashMap<String, String>();				
				
				for (int i = 0; i < indices.length; i++) {
					String name = inSpec.getColumnSpec(indices[i]).getName();
					String cls = ((StringCell)row.getCell(indices[i])).getStringValue();
					String suffix = name.endsWith("-180") ? "-180" : "";
					
					double score = WEIGHTS.get(types[i])[getIndex(cls)];
					
					if(probIndices[i] != -1)
					{
						score *= ((DoubleCell)row.getCell(probIndices[i])).getDoubleValue();
					}					
					
					prediction.put(name, cls);
					
					cls += suffix;
					
					if (scores.containsKey(cls))
					{
						scores.get(cls).add(score);
					}
					else
					{
						scores.put(cls, new MutableDouble(score));
					}
				}
				
				String best = null;
				Double bestScore = 0d;
				for (Entry<String, MutableDouble> entry  : scores.entrySet())
				{
					if (entry.getValue().doubleValue() > bestScore)
					{
						best = entry.getKey();
						bestScore = entry.getValue().doubleValue();
					}
				}
				
				if (best.equals("W") && prediction.get("Angular").equals("M") )
				{
					best = "M";
				}
				else if (best.equals("M") && prediction.get("Angular").equals("W") )
				{
					best = "W";
				}
				else if ((best.equals("0") || best.equals("O")) && prediction.get("Radial").equals(".") )
				{
					best = ".";
				}
				else if (best.equals("7") && (prediction.get("Angular").equals("I") || prediction.get("Angular").equals("T")))
				{
					best = prediction.get("Angular");
				}
				else if (best.equals("C") && prediction.get("Vertical").equals("G"))
				{
					best = "G";
				}
				else if (best.equals("G") && prediction.get("Vertical").equals("C"))
				{
					best = "C";
				}
				
				return new StringCell(best);
			}
		});
		
		return columnRearranger;
	}

    /**
     * {@inheritDoc}
     */
    @Override
    protected BufferedDataTable[] execute(final BufferedDataTable[] inData,
            final ExecutionContext exec) throws Exception
    {
    	final List<String> columns = settings.getIncludes();
    	final int count = columns.size();
    	final int[] indices = new int[count];
    	final int[] probIndices = new int[count];
    	final String[] types = new String[count];

    	DataTableSpec spec = inData[0].getDataTableSpec();
    	
    	int i = 0;
    	for (String column : columns)
    	{
    		types[i] = column.split("-")[0];
    		probIndices[i] = spec.findColumnIndex(column + "-P");
    		indices[i++] = spec.findColumnIndex(column);
		}
    	
    	ColumnRearranger cr = getColumnRearranger(spec, indices, probIndices, types);
        
        BufferedDataTable table = exec.createColumnRearrangeTable(inData[0], cr, exec);
    	
        return new BufferedDataTable[]{ table };
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void reset() 
    {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected DataTableSpec[] configure(final DataTableSpec[] inSpecs)
            throws InvalidSettingsException
    {
        return new DataTableSpec[]{ getColumnRearranger(inSpecs[0], null, null, null).createSpec() };
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void saveSettingsTo(final NodeSettingsWO settings)
    {
    	this.settings.saveSettings(settings);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void loadValidatedSettingsFrom(final NodeSettingsRO settings)
            throws InvalidSettingsException
    {
    	this.settings.loadSettings(settings);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void validateSettings(final NodeSettingsRO settings)
            throws InvalidSettingsException            
    {
    	this.settings.validateSettings(settings);
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    protected void loadInternals(final File internDir,
            final ExecutionMonitor exec) throws IOException,
            CanceledExecutionException            
    {
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    protected void saveInternals(final File internDir,
            final ExecutionMonitor exec) throws IOException,
            CanceledExecutionException
    {
    }

}

