package org.knimelab.ocr.extractor;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.knime.core.data.DataCell;
import org.knime.core.data.DataColumnSpec;
import org.knime.core.data.DataColumnSpecCreator;
import org.knime.core.data.DataRow;
import org.knime.core.data.DataTableSpec;
import org.knime.core.data.DataType;
import org.knime.core.data.RowKey;
import org.knime.core.data.def.DefaultRow;
import org.knime.core.data.def.DoubleCell;
import org.knime.core.data.def.IntCell;
import org.knime.core.data.image.png.PNGImageCell;
import org.knime.core.data.image.png.PNGImageContent;
import org.knime.core.data.image.png.PNGImageValue;
import org.knime.core.node.BufferedDataContainer;
import org.knime.core.node.BufferedDataTable;
import org.knime.core.node.CanceledExecutionException;
import org.knime.core.node.ExecutionContext;
import org.knime.core.node.ExecutionMonitor;
import org.knime.core.node.InvalidSettingsException;
import org.knime.core.node.NodeModel;
import org.knime.core.node.NodeSettingsRO;
import org.knime.core.node.NodeSettingsWO;


/**
 * This is the model implementation of ExtractLetters.
 * This node extracts letters of a PNG-Image and gives back all the letters as PNG-Images
 *
 * @author Knime Lab Group
 */
public class ExtractLettersNodeModel extends NodeModel {    
    private static int tmp_enumerator_count = 0;
    private int tmp_enumerator; 
    private ExtractLettersNodeSettings settings = new ExtractLettersNodeSettings();

    synchronized void setTmpEnumerator(){
    	tmp_enumerator = tmp_enumerator_count++;    	
    }
    
    /**
     * Constructor for the node model.
     */
    protected ExtractLettersNodeModel() {
        super(1, 1);

        setTmpEnumerator();
    }

    private DataColumnSpec[] getColSpec(){
    	
    	DataColumnSpec[] allColSpecs;
    	if(settings.getExtraInfo()){
    		allColSpecs = new DataColumnSpec[6];
    		allColSpecs[3] = new DataColumnSpecCreator("Height", IntCell.TYPE).createSpec();
    		allColSpecs[4] = new DataColumnSpecCreator("Width", IntCell.TYPE).createSpec();
    		allColSpecs[5] = new DataColumnSpecCreator("Height/Width", DoubleCell.TYPE).createSpec();
    	}else{
    		allColSpecs = new DataColumnSpec[3];
    	}
        allColSpecs[0] = new DataColumnSpecCreator("Image", DataType.getType(PNGImageCell.class)).createSpec();
        allColSpecs[1] = new DataColumnSpecCreator("X", IntCell.TYPE).createSpec();
        allColSpecs[2] = new DataColumnSpecCreator("Y", IntCell.TYPE).createSpec();
        return allColSpecs;
    }
    
    private boolean[][] testedGrid;
    
    /**
     * {@inheritDoc}
     */
    @Override
    protected BufferedDataTable[] execute(final BufferedDataTable[] inData,
            final ExecutionContext exec) throws Exception
    {        
        BufferedImage image;
        if(inData[0].getDataTableSpec().containsName("Image") == false){
        	throw new Exception( " No Column named Image " );
        }
        
        int imageIndex = inData[0].getDataTableSpec().findColumnIndex("Image");
        int numOfRows = inData[0].getRowCount();
        int rowCount = 0;
        
        // the data table spec of the single output table, 
        // the table will have one columns:
        DataColumnSpec[] allColSpecs = getColSpec();

        DataTableSpec outputSpec = new DataTableSpec(allColSpecs);
        
        // the execution context will provide us with storage capacity, in this
        // case a data container to which we will add rows sequentially
        // Note, this container can also handle arbitrary big data tables, it
        // will buffer to disc if necessary.
        BufferedDataContainer container = exec.createDataContainer(outputSpec);
        
        for(DataRow row : inData[0]){
	        image = (BufferedImage) ((PNGImageValue) row.getCell(imageIndex)).getImageContent().getImage(); 
	        
	        int count = 0;
	        testedGrid = new boolean[image.getWidth()][image.getHeight()];
	        for (int y = 0; y < image.getHeight(); y++) {
	        	for (int x = 0; x <image.getWidth(); x++ ){
	        		testedGrid[x][y] = false;
	        	}
	        }
	        
	        int subcount = 0;
	        
	        int cur_height;
	        for (int y = 0; y < image.getHeight(); y += 5) {
	        	cur_height = 5;
	        	for (int x = 0; x <image.getWidth(); x++ ){
	        		for (int height_ = 0; height_ < cur_height && y + height_ < image.getHeight(); height_++){
			        	if ( new Color(image.getRGB(x, y+height_)).getBlue() != 255 && testedGrid[x][y+height_] == false){
			        		
			        		// Explore found Letter. It returns [xMin,xMax,yMin,yMax]
			        		int[] ex = explore(image, x, y + height_);
			        		int xMin = ex[0], 
			        			yMin = ex[2],
			        			height = ex[3]-ex[2],
			        			width = ex[1]-ex[0];
			        		
			        		if((height > 3 || width > 3) ){ // && height > 0 && width > 0
					        	cur_height = Math.max(height+5, cur_height);	
	
					            File tmp = File.createTempFile(tmp_enumerator + "knime_tmp_file" + count, ".png");
					            ImageIO.write(image.getSubimage(xMin, yMin, width+1, height+1), "png", tmp);
					            
					            
					            // the cells of the current row, the types of the cells must match
							    // the column spec (see above)
					            DataCell[] cells;
					            if(settings.getExtraInfo()){
					            	cells = new DataCell[6];
					            	cells[3] = new IntCell(height+1);
					            	cells[4] = new IntCell(width+1);
					            	cells[5] = new DoubleCell(((double)height+1)/((double)width+1));
					            }else{
					            	cells = new DataCell[3];
					            }
					            cells[0] = (new PNGImageContent(new FileInputStream(tmp))).toImageCell(); 
					            cells[1] = new IntCell(xMin); 
					            cells[2] = new IntCell(yMin);
					         
					            
					            String old_key = row.getKey().toString();					            
					            RowKey new_key = new RowKey(old_key + (subcount++ == 0? "" : " #" + subcount));
							            
						        DataRow addrow = new DefaultRow(new_key, cells);
							    container.addRowToTable(addrow);
							    
							    count++;
			        		}
			        	}
	        		} 
	        	}
	        	exec.checkCanceled();
	        	if(numOfRows == 1){
				    exec.setProgress(1 / (numOfRows-rowCount), 
					        "Adding row " + count);	        		
	        	} else {
				    exec.setProgress(y / (image.getHeight()), 
					        "Adding row " + count);
	        	}

	        }
	        rowCount++;
        }
        
        // once we are done, we close the container and return its table
        container.close();
        BufferedDataTable out = container.getTable();
        return new BufferedDataTable[]{out};
    }
	
    // Explore found Letter. It returns [xMin,xMax,yMin,yMax]
    private int[] explore(BufferedImage image, int x, int y) {
    	if(        x>0 && x<image.getWidth() && y>0 && y<image.getHeight() 
			    && testedGrid[x][y] == false
				&& new Color(image.getRGB(x, y)).getBlue() < settings.getThreshold() ){
    		testedGrid[x][y] = true;

			int[][] scores = new int[][]{
					explore(image, x-1, y   ), // left
					explore(image, x+1, y   ), // right
					explore(image, x  , y-1 ), // up
					explore(image, x  , y+1 ), // down
					explore(image, x-1, y-1 ), // left-up
					explore(image, x-1, y+1 ), // left-down
					explore(image, x+1, y-1 ), // right-up
					explore(image, x+1, y+1 )  // right-down
			};
			
			int[] result = new int[]{
					x, x, y, y
			};
			
			for(int i=0; i<4; i++){
				for(int j=0; j<scores.length; j++){
					result[i] = ( (i%2==0) 
							  ? Math.min(result[i], scores[j][i]) : Math.max(result[i], scores[j][i]));
				}
			}

			return result;
		} else 
			return new int[]{image.getWidth(), 0, image.getHeight(), 0}; // be sure not to use this..
	}

	/**
     * {@inheritDoc}
     */
    @Override
    protected void reset() {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected DataTableSpec[] configure(final DataTableSpec[] inSpecs)
            throws InvalidSettingsException {
        return new DataTableSpec[]{new DataTableSpec(getColSpec())};
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void saveSettingsTo(final NodeSettingsWO settings)
    {
        this.settings.saveSettings(settings);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void loadValidatedSettingsFrom(final NodeSettingsRO settings)
            throws InvalidSettingsException
    {
        this.settings.loadSettings(settings);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void validateSettings(final NodeSettingsRO settings)
            throws InvalidSettingsException
    {
        this.settings.validateSettings(settings);
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    protected void loadInternals(final File internDir,
            final ExecutionMonitor exec) throws IOException,
            CanceledExecutionException {
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    protected void saveInternals(final File internDir,
            final ExecutionMonitor exec) throws IOException,
            CanceledExecutionException {
    }

}

