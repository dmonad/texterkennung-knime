package org.knimelab.ocr.extractor;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.TreeSet;
import java.util.Vector;

import javax.imageio.ImageIO;

import org.knime.core.data.DataCell;
import org.knime.core.data.DataColumnSpec;
import org.knime.core.data.DataColumnSpecCreator;
import org.knime.core.data.DataRow;
import org.knime.core.data.DataTableSpec;
import org.knime.core.data.DataType;
import org.knime.core.data.def.DefaultRow;
import org.knime.core.data.def.DoubleCell;
import org.knime.core.data.def.IntCell;
import org.knime.core.data.def.StringCell;
import org.knime.core.data.image.png.PNGImageCell;
import org.knime.core.data.image.png.PNGImageContent;
import org.knime.core.data.image.png.PNGImageValue;
import org.knime.core.node.BufferedDataContainer;
import org.knime.core.node.BufferedDataTable;
import org.knime.core.node.CanceledExecutionException;
import org.knime.core.node.ExecutionContext;
import org.knime.core.node.ExecutionMonitor;
import org.knime.core.node.InvalidSettingsException;
import org.knime.core.node.NodeLogger;
import org.knime.core.node.NodeModel;
import org.knime.core.node.NodeSettingsRO;
import org.knime.core.node.NodeSettingsWO;

class Point
{
	public Point(double x, double y, int c)
	{
		this.x = x;
		this.y = y;
		this.c = c;
	}

	public final double x;
	public final double y;
	public final int c;
}

class Box {
    public double Axis1X = 0, Axis1Y = 0;
    public double Axis2X = 0, Axis2Y = 0;
    public double Extent1 = 1, Extent2 = 0;
    public double CenterX = 0, CenterY = 0;
}

class Area
{
	public final TreeSet<Point> points = new TreeSet<Point>(new Comparator<Point>()
	{
		@Override
		public int compare(Point arg0, Point arg1)
		{
			int x = Double.compare(arg0.x, arg1.x);
			return x == 0? Double.compare(arg0.y, arg1.y) : x;
		}
	});
	
	public int minX = Integer.MAX_VALUE;
	public int maxX = Integer.MIN_VALUE;
	public int minY = Integer.MAX_VALUE;
	public int maxY = Integer.MIN_VALUE;
	
	public Point[] hull;
	
	public int first = 0, second = 0;
	
	public double rotation;
	public double rotationWidth;
	public double rotationHeight;
	
	public long totalDarkness = 0;
	
	public int getWidth()
	{
		return maxX - minX + 1;
	}
	
	public int getHeight()
	{
		return maxY - minY + 1;
	}
	
	public void addPoint(double x, double y, int c)
	{
		addPoint(new Point(x, y, c));
	}
	
	public void addPoint(final Point p)
	{
		if (p.x < minX) minX = (int)Math.floor(p.x);
		if (p.x > maxX) maxX = (int)Math.ceil(p.x);
		if (p.y < minY) minY = (int)Math.floor(p.y);
		if (p.y > maxY) maxY = (int)Math.ceil(p.y);
		
		totalDarkness += 255 - p.c;
		
		points.add(p);
	}
    
    private double cross(Point o, Point a, Point b)
    {
        return (a.x - o.x) * (b.y - o.y) - (a.y - o.y) * (b.x - o.x);
    }
    
    private double dot(double x1, double y1, double x2, double y2)
    {
    	return x1 * x2 + y1 * y2;
    }
    
    private Point findStart(BufferedImage image)
    {
		int width = image.getWidth();
		int height = image.getHeight();
		
        for(int y = 0; y < height; y ++)
        {
          for(int x = 0; x < width; x ++)
          {
        	  Color color = new Color(image.getRGB(x, y));
        	  
        	  if (color.getBlue() != 255)
        	  {
        		  return new Point(x, y, color.getBlue());
        	  }
          }
        }
        
        return null;
    }
    
    private Point getWeightedPoint(BufferedImage image, int x, int y)
    {
    	double px = 0, py = 0;
    	double weight = 0;
    	
    	for (int i = -1; i <= 1; i++)
    	{
        	for (int j = -1; j <= 1; j++)
        	{
        		Color c = new Color(image.getRGB(x + i, y + j));
        		
        		weight += c.getBlue();
        		px += (x + i) * c.getBlue();
        		py += (y + j) * c.getBlue();
        	}
    	}
    	
		Color c = new Color(image.getRGB(x, y));
		
		return new Point(px / weight - 1, py / weight - 1, c.getBlue());
    }
    
    public void mooreNeighborTracing()
    {
		int width = getWidth();
		int height = getHeight();

		BufferedImage image = new BufferedImage(width + 2, height + 2,
				BufferedImage.TYPE_INT_RGB);
		Graphics g = image.createGraphics();

		g.setColor(Color.WHITE);
		g.fillRect(0, 0, width + 2, height + 2);

		for (Point point : points) {
			g.setColor(new Color(point.c, point.c, point.c));
			g.fillRect((int) point.x - minX + 1, (int) point.y - minY + 1, 1, 1);
		}

		g.dispose();

		Vector<Point> hull = new Vector<Point>();

		Point start = findStart(image);
		int pos = ((int) start.x) + ((int) start.y) * (width + 2);

		hull.add(start);

		int[] paddedImage = image.getRGB(0, 0, width + 2, height + 2, null, 0,
				width + 2);

		int checkLocationNr = 1; // The neighbor number of the location we want
								 // to check for a new border point
		int checkPosition; // The corresponding absolute array address of
						   // checkLocationNr
		int newCheckLocationNr; // Variable that holds the neighborhood position
								// we want to check if we find a new border at
								// checkLocationNr
		int startPos = pos; // Set start position
		int counter = 0; // Counter is used for the jacobi stop criterion
		int counter2 = 0; // Counter2 is used to determine if the point we have
						  // discovered is one single point

		// Defines the neighborhood offset position from current position and
		// the neighborhood
		// position we want to check next if we find a new border at
		// checkLocationNr
		int neighborhood[][] = new int[][]
		{
				{ -1, 7 },
				{ -3 - width, 7 },
				{ -width - 2, 1 },
				{ -1 - width, 1 },
				{ 1, 3 },
				{ 3 + width, 3 },
				{ width + 2, 5 },
				{ 1 + width, 5 }
		};
		
		// Trace around the neighborhood
		while (true)
		{
			checkPosition = pos + neighborhood[checkLocationNr - 1][0];
			newCheckLocationNr = neighborhood[checkLocationNr - 1][1];

			Color c = new Color(paddedImage[checkPosition]);

			if (c.getBlue() != 255) // Next border point found
			{
				if (checkPosition == startPos)
				{
					counter++;

					// Stopping criterion (jacob)
					if (newCheckLocationNr == 1 || counter >= 3)
					{
						// Close loop
						break;
					}
				}

				checkLocationNr = newCheckLocationNr; // Update which
													  // neighborhood position
													  // we should check next
				pos = checkPosition;
				counter2 = 0; // Reset the counter that keeps track of how many
							  // neighbors we have visited
				
				hull.add(getWeightedPoint(
					image,
					checkPosition % (width + 2),
					checkPosition / (width + 2)
				));
			}
			else
			{
				// Rotate clockwise in the neighborhood
				checkLocationNr = 1 + (checkLocationNr % 8);
				if (counter2 > 8) {
					// If counter2 is above 8 we have traced around the
					// neighborhood and
					// therefor the border is a single black pixel and we can
					// exit
					counter2 = 0;
					break;
				} else {
					counter2++;
				}
			}
		}

		buildHull(hull);
    }
    
    private static Point intersectLines(Point a, Point b, Point c, Point d)
    {
        double x1 = a.x;
        double x2 = b.x;
        double x3 = c.x;
        double x4 = d.x;
        double y1 = a.y;
        double y2 = b.y;
        double y3 = c.y;
        double y4 = d.y;

        double zx = (x1 * y2 - y1 * x2) * (x3 - x4) - (x1 - x2) * (x3 * y4 - y3 * x4);
        double zy = (x1 * y2 - y1 * x2) * (y3 - y4) - (y1 - y2) * (x3 * y4 - y3 * x4);
          
        double n = (x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4);
        
        double x = zx/n;
        double y = zy/n;

        return new Point(x, y, 0);
    }
    
    private void buildHull(Collection<Point> thePoints)
    {
    	TreeSet<Point> points = new TreeSet<Point>(new Comparator<Point>() {
    		@Override
    		public int compare(Point arg0, Point arg1)
    		{
    			int x = Double.compare(arg0.x, arg1.x);
    			return x == 0? Double.compare(arg0.y, arg1.y) : x;
    		}
    	});
    	
    	points.addAll(thePoints);
    	
    	if (points.size() == 0)
    	{
    		hull = new Point[0];
    		return;
    	}
    	
    	if (points.size() == 1)
    	{
    		hull = new Point[] {points.iterator().next()};
    		return;
    	}
    	
    	Vector<Point> lower = new Vector<Point>();
    	for (Point p : points) {
    		if (p.c > 100) continue;
    		 while (lower.size() >= 2 && cross(lower.get(lower.size()-2), lower.get(lower.size()-1), p) <= 0)
		            lower.remove(lower.size() - 1);
		        lower.add(p);
		}
    	
    	Vector<Point> upper = new Vector<Point>();
    	Iterator<Point> iter = points.descendingIterator();
    	while (iter.hasNext()) {
    		Point p = iter.next();
    		if (p.c > 100) continue;
    		 while (upper.size() >= 2 && cross(upper.get(upper.size()-2), upper.get(upper.size()-1), p) <= 0)
    			 upper.remove(upper.size() - 1);
    		 upper.add(p);
		}
    	
    	int size = lower.size()+upper.size()-2;
    	if (size < 0) size = 0;
    	hull = new Point[size];
    	
    	int i = 0;
    	for (; i < lower.size() - 1; i++) {
			hull[i] = lower.get(i);
		}
    	for (int j = 0; i < hull.length; i++, j++) {
			hull[i] = upper.get(j);
		}
    	
    	if (hull.length > 2)
    	{
    		hull = douglasPeucker(hull, 1.2, 0, hull.length-1);
    	}
    	
    	double cmax = Math.min(0.2 * 0.2 * getWidth() * getHeight(), 8 * 8);
    	double cmin = 0.25 * cmax;
    	
    	unclipEdges(cmax, cmin, 0.5);
    }
    
    private void unclipEdges(double maxLength, double minLength, double maxDot)
    {    	
    	ArrayList<Point> newPoints = new ArrayList<Point>(Arrays.asList(hull));
    	
    	int i = 0;
    	while (i < newPoints.size())
    	{
    		int bi = (i + 1) % newPoints.size();
    		int ci = (i + 2) % newPoints.size();
    		int di = (i + 3) % newPoints.size();
    		Point a = newPoints.get(i);
    		Point b = newPoints.get(bi);
    		Point c = newPoints.get(ci);
    		Point d = newPoints.get(di);
    		
    		double dx1 = b.x - a.x;
    		double dy1 = b.y - a.y;
    		double dx2 = c.x - b.x;
    		double dy2 = c.y - b.y;
    		double dx3 = d.x - c.x;
    		double dy3 = d.y - c.y;
    		
    		double l1 = dx1 * dx1 + dy1 * dy1;
    		double l2 = dx2 * dx2 + dy2 * dy2;
    		double l3 = dx3 * dx3 + dy3 * dy3;
    		double dot = dot(dx1, dy1, dx3, dy3);
    		
    		if (l1 >= maxLength && l2 <= minLength && l3 >= maxLength && Math.abs(dot) < maxDot)
    		{
    			newPoints.remove(b);
    			newPoints.remove(c);
    			newPoints.add((i + 1) % (newPoints.size() + 1), intersectLines(a, b, c, d));
    		}
    		else
    		{
        		i++;				
			}
    	}
    	
    	hull = newPoints.toArray(new Point[0]);
    }
    
	private static double perpendicularDistance(Point point, Point lineStart, Point lineEnd)
	{
		double area = Math.abs(
		(
			1.0 * lineStart.x * lineEnd.y
			+ 1.0 * lineEnd.x * point.y
			+ 1.0 * point.x * lineStart.y
			- 1.0 * lineEnd.x * lineStart.y
			- 1.0 * point.x * lineEnd.y
			- 1.0 * lineStart.x * point.y
			) / 2.0
		);
		
		double bottom = Math.hypot(
			lineStart.x - lineEnd.x,
			lineStart.y - lineEnd.y
		);
		
		return (area / bottom * 2.0);
	}
	
	private Point[] douglasPeucker(Point[] points, double eps, int start, int end)
	{
	    // Find the point with the maximum distance
	    double dmax = 0;
	    int index = 0;
	    for (int i = start + 1; i < end; i++)
	    {
	        double d = perpendicularDistance(points[i], points[start], points[end]);
	        if (d > dmax)
	        {
	            index = i;
	            dmax = d;
	        }
	    }
	    
	    // If max distance is greater than epsilon, recursively simplify
	    if (dmax > eps)
	    {
	        // Recursive call
	    	Point[] recResults1 = douglasPeucker(points, eps, start, index);
	    	Point[] recResults2 = douglasPeucker(points, eps, index, end);	    	
	    	   
	    	Point[] ResultList = new Point[recResults1.length + recResults2.length - 1];

	    	System.arraycopy(recResults1, 0, ResultList, 0, recResults1.length - 1);
	    	System.arraycopy(recResults2, 0, ResultList, recResults1.length - 1, recResults2.length);
	 
	    	return ResultList;
	    }
	    
	    return new Point[]{ points[start], points[end] };
	}
    
    public void normalizeRotation()
    {
    	final int n = hull.length;
    	double minimumArea = Double.POSITIVE_INFINITY;
    	double d0x = 0, d0y = 0;
    	for (int i = 0; i < n; i++)
    	{
    		double u0x = hull[(i + 1) % n].x - hull[i].x;
    		double u0y = hull[(i + 1) % n].y - hull[i].y;
    		double u0l = Math.sqrt(u0x * u0x + u0y * u0y);
    		u0x /= u0l;
    		u0y /= u0l;
    		
    		double u1x = -u0y;
    		double u1y = u0x;
    		
	    	double s0 = 0, t0 = 0, s1 = 0, t1 = 0;
	    	for (int j = 1; j < n; j++)
	    	{
		    	double dx = hull[j].x - hull[0].x;
		    	double dy = hull[j].y - hull[0].y;
		    	
		    	double test = dot(u0x, u0y, dx, dy);
		    	if (test < s0)
		    	{
		    		s0 = test;
		    	}
		    	else if (test > s1)
		    	{
		    		s1 = test;
		    	}
		    	
		    	test = dot(u1x, u1y, dx, dy);
		    	if (test < t0)
		    	{
		    		t0 = test;
		    	}
		    	else if (test > t1)
		    	{
		    		t1 = test;
		    	}
	    	}
	    	
	    	double area = (s1-s0)*(t1-t0);
	    	if (area < minimumArea)
	    	{
	    		minimumArea = area;
	    		first = i;
	    		second = (i + 1) % n;
	    		rotationWidth = t1-t0;
	    		rotationHeight = s1-s0;
	    		d0x = u0x;
	    		d0y = u0y;
	    	}
    	}
    	
    	double angle1 = Math.acos(d0x);
    	double angle2 = Math.acos(d0y);
    	
    	rotation = angle2;

    	// Weird bug for certain angles, swap width and height
    	if (angle1 > angle2 && angle1 + angle2 > (Math.PI*5/6d) && angle1 + angle2 < Math.PI)
    	{
    		double tmp = rotationWidth;
    		rotationWidth = rotationHeight;
    		rotationHeight = tmp;
    	}
    	
    	rotate();
    }
    
    private void rotate()
    {    	
    	final double EPS = 1E-7;
    	
    	boolean turn90degrees = rotationWidth > rotationHeight;

    	if (turn90degrees)
    	{
    		rotation += Math.PI / 2;
    	}
    	
    	rotation %= Math.PI;
    	
    	if ((Math.PI - rotation > EPS && rotation > EPS) || turn90degrees)
    	{
	    	Vector<Point> origPoints = new Vector<Point>(points.size());
	    	origPoints.addAll(points);	  
	    	reset();	    	
	    	
	    	double cx = (minX + maxX + 1) / 2;
	    	double cy = (minY + maxY + 1) / 2;
	    	double cos = Math.cos(rotation);
	    	double sin = Math.sin(rotation);
	    	
	    	for (Point point : origPoints)
	    	{
				double x = point.x - cx;
				double y = point.y - cy;
				
				double nx = x * cos - y * sin;
				double ny = x * sin + y * cos;
				
				nx += cx;
				ny += cy;
				
				addPoint(nx, ny, point.c);
			}
    	}
    }
    
    public void reset()
    {  	
    	points.clear();
    	totalDarkness = 0;
    	minX = minY = Integer.MAX_VALUE;
    	maxX = maxY = Integer.MIN_VALUE;    	
    }
}

/**
 * This is the model implementation of FloodFillExtractor.
 * 
 *
 * @author Team kNN
 */
public class FloodFillExtractorNodeModel extends NodeModel
{    
	final FloodFillExtractorNodeSettings settings = new FloodFillExtractorNodeSettings();
	
	public static final NodeLogger logger = NodeLogger.getLogger(FloodFillExtractorNodeModel.class);

	/**
     * Constructor for the node model.
     */
    protected FloodFillExtractorNodeModel()
    {
        super(1, 1);
    }
    
    private DataTableSpec getSpec()
    {
        DataColumnSpec[] allColSpecs = new DataColumnSpec[3 + (settings.getPath() ? 1 : 0) + (settings.normRotation() ? 1 : 0) 
                                                          + (settings.isAddHeightWidthProportion() ? 1 : 0)];
        allColSpecs[0] = new DataColumnSpecCreator("Image", DataType.getType(PNGImageCell.class)).createSpec();
        allColSpecs[1] = new DataColumnSpecCreator("X", IntCell.TYPE).createSpec();
        allColSpecs[2] = new DataColumnSpecCreator("Y", IntCell.TYPE).createSpec();
        int index = 3;
        if (settings.normRotation())
        	allColSpecs[index++] = new DataColumnSpecCreator("Rotation", IntCell.TYPE).createSpec();
        if (settings.getPath())
        	allColSpecs[index++] = new DataColumnSpecCreator("Path", StringCell.TYPE).createSpec();
        if (settings.isAddHeightWidthProportion())
        	allColSpecs[index] = new DataColumnSpecCreator("heightWidth-proportion", DoubleCell.TYPE).createSpec();

        return new DataTableSpec(allColSpecs);
    }
    
    private void drawPixel(BufferedImage img, double x, double y, int mag)
    {
    	Graphics g = img.createGraphics();
    	int ix = (int)x;
    	int iy = (int)y;
    	double hf = 1 - (x - ix);
    	double vf = 1 - (y - iy);
    	
    	if (ix == x)
    	{
    		if (iy == y)
    		{
    			g.setColor(new Color(255 - mag, 255 - mag, 255 - mag));
    			g.fillRect(ix, iy, 1, 1);
    		}
    		else
    		{
    			Color tColor = new Color(img.getRGB(ix, iy));
    			Color bColor = new Color(img.getRGB(ix, iy+1));
    			
    			int magnitude = (int)(tColor.getBlue() - mag * vf);
    			if (magnitude < 0) magnitude = 0;
    			
    			g.setColor(new Color(magnitude, magnitude, magnitude));
    			g.fillRect(ix, iy, 1, 1);
    			
    			magnitude = (int)(bColor.getBlue() - mag * (1 - vf));
    			if (magnitude < 0) magnitude = 0;
    			
    			g.setColor(new Color(magnitude, magnitude, magnitude));
    			g.fillRect(ix, iy+1, 1, 1);				
			}
    	}
    	else if (iy == y)
    	{
			Color lColor = new Color(img.getRGB(ix, iy));
			Color rColor = new Color(img.getRGB(ix+1, iy));
			
			int magnitude = (int)(lColor.getBlue() - mag * hf);
			if (magnitude < 0) magnitude = 0;
			
			g.setColor(new Color(magnitude, magnitude, magnitude));
			g.fillRect(ix, iy, 1, 1);
			
			magnitude = (int)(rColor.getBlue() - mag * (1 - hf));
			if (magnitude < 0) magnitude = 0;
			
			g.setColor(new Color(magnitude, magnitude, magnitude));
			g.fillRect(ix+1, iy, 1, 1);		    		
    	}
    	else
    	{
			Color tlColor = new Color(img.getRGB(ix, iy));
			Color trColor = new Color(img.getRGB(ix+1, iy));
			Color blColor = new Color(img.getRGB(ix, iy+1));
			Color brColor = new Color(img.getRGB(ix+1, iy+1));
			
			int magnitude = (int)(tlColor.getBlue() - mag * hf * vf);
			if (magnitude < 0) magnitude = 0;
			
			g.setColor(new Color(magnitude, magnitude, magnitude));
			g.fillRect(ix, iy, 1, 1);
			
			magnitude = (int)(trColor.getBlue() - mag * (1 - hf) * vf);
			if (magnitude < 0) magnitude = 0;
			
			g.setColor(new Color(magnitude, magnitude, magnitude));
			g.fillRect(ix+1, iy, 1, 1);	  
			
			magnitude = (int)(blColor.getBlue() - mag * hf * (1 - vf));
			if (magnitude < 0) magnitude = 0;
			
			g.setColor(new Color(magnitude, magnitude, magnitude));
			g.fillRect(ix, iy + 1, 1, 1);
			
			magnitude = (int)(brColor.getBlue() - mag * (1 - hf) * (1 - vf));
			if (magnitude < 0) magnitude = 0;
			
			g.setColor(new Color(magnitude, magnitude, magnitude));
			g.fillRect(ix+1, iy + 1, 1, 1);	    		
    	}
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected BufferedDataTable[] execute (final BufferedDataTable[] inData,
            final ExecutionContext exec) throws Exception
    {
        BufferedImage image;
        if (!inData[0].getDataTableSpec().containsName("Image"))
        {
        	throw new Exception("No column named \"Image\"");
        }
        
        final int imageIndex = inData[0].getDataTableSpec().findColumnIndex("Image");
        final int numOfRows = inData[0].getRowCount();
        
        final int treshold = settings.getThreshold();
        final int minWidth = settings.getMinWidth();
        final int minHeight = settings.getMinHeight();
 
        BufferedDataContainer container = exec.createDataContainer(getSpec());
        
        final double f = 1d / numOfRows;
        double progress = 0;
        int count = 0;
        
        for (DataRow row : inData[0])
        {
        	progress = ((double)count) / numOfRows;
        	
        	exec.checkCanceled();
        	exec.setProgress(progress, "Row " + count++ + " of " + numOfRows);
        	
        	int i = 0;
	        image = (BufferedImage) ((PNGImageValue) row.getCell(imageIndex)).getImageContent().getImage();
	        
	        final int width = image.getWidth();
	        final int height = image.getHeight();
	        
	        final boolean[][] visited = new boolean[width][height];	        
	        final double total = width * height;
	        
	        for (int x = 0; x < width; x++)
	        {
		        for (int y = 0; y < height; y++)
		        {
		        	if (!visited[x][y])
		        	{
		        		Area area = FloodFill(image, x, y, treshold, visited);
		        		
		        		if (area == null)
		        		{	        			
		        			continue;
		        		}
		        		
		        		if (area.totalDarkness < 800 || area.points.size() < 6)
		        		{
		        			continue;
		        		}
		        		
		        		int origMinX = area.minX;
		        		int origMinY = area.minY;
		        		
		        		if (settings.normRotation())
		        		{
			        		area.mooreNeighborTracing();
			        		area.normalizeRotation();
		        		}
		        		
		        		final int aw = area.getWidth();
		        		final int ah = area.getHeight();
		        		
		        		if (aw >= minWidth && ah >= minHeight)
		        		{			        		
			        		BufferedImage targetImage = new BufferedImage(aw, ah, BufferedImage.TYPE_INT_RGB);			        		
							Graphics g = targetImage.createGraphics();
							
		        			g.setColor(Color.WHITE);
							g.fillRect(0, 0, aw, ah);
			        		
							for (final Point point : area.points)
							{								
								drawPixel(targetImage, point.x - area.minX, point.y - area.minY, 255 - point.c);
							}
							
							/*
							for (int k = 0; k < area.hull.length; k++)
							{
								Point startPoint = area.hull[k];
								Point endPoint = area.hull[(k+1)%area.hull.length];
								g.setColor(Color.RED);
								((Graphics2D)g).draw(new Line2D.Double(startPoint.x, startPoint.y, endPoint.x, endPoint.y));
								g.setColor(Color.GREEN);
								((Graphics2D)g).draw(new Rectangle2D.Double(startPoint.x, startPoint.y, 0.5, 0.5));
								((Graphics2D)g).draw(new Rectangle2D.Double(endPoint.x, endPoint.y, 0.5, 0.5));
							}
							*/
							
		        			g.dispose();
	
		        			final String key = row.getKey().toString() + (i++ == 0? "" : " #" + i);
		        			
		        			final File tmp = File.createTempFile("knime_flood_fill_" + key, ".png");
				            ImageIO.write(targetImage, "png", tmp);

				            final DataCell[] cells = 
				            		new DataCell[3 + (settings.getPath() ? 1 : 0) 
				            		             + (settings.normRotation() ? 1 : 0) 
				            		             + (settings.isAddHeightWidthProportion() ? 1 : 0)];
				            cells[0] = new PNGImageContent(new FileInputStream(tmp)).toImageCell();
				            cells[1] = new IntCell(origMinX);
				            cells[2] = new IntCell(origMinY);
				            int index = 3;
				            if (settings.normRotation())
				            	cells[index++] = new IntCell((int)Math.toDegrees(area.rotation));
				            if (settings.getPath())
				            	cells[index++] = new StringCell(tmp.getAbsolutePath());
				            if (settings.isAddHeightWidthProportion())
				            	cells[index] = new DoubleCell(((double)area.getHeight())/((double)area.getWidth()));

						    container.addRowToTable(new DefaultRow(key, cells));									        			
		        		}
		        	}
		        	
		        	exec.checkCanceled();		        	
				    exec.setProgress(
				    	progress + ((x * height + y) / total) * f, 
				    	"Row " + count + " of " + numOfRows
				    );	
		        }	        	
	        }	        
        }
        
        container.close();    	
    	
        return new BufferedDataTable[]{ container.getTable() };
    }
    
    private int getColor(final BufferedImage image, final int x, final int y)
    {
    	return new Color(image.getRGB(x, y)).getBlue();
    }
    
    private Area FloodFill(final BufferedImage image, final int x, final int y, final int treshold, final boolean[][] visited)
    {
		visited[x][y] = true;
		
    	int pixel = getColor(image, x, y);
    	if (pixel >= treshold)
    	{
    		return null;
    	}
    	
    	Area area = new Area();
    	LinkedList<Point> queue = new LinkedList<Point>();
    	
    	final int width = image.getWidth();
    	final int height = image.getHeight();
    	
    	queue.add(new Point(x, y, pixel));
    	
    	while (!queue.isEmpty())
    	{
    		Point current = queue.poll();
    		area.addPoint(current);
    		
    		int w, wx = (int)current.x - 1, cy = (int)current.y;    		
    		for (w = wx; w >= 0 && !visited[w][cy]; w--)
    		{
    			pixel = getColor(image, w, cy);
    			visited[w][cy] = true;
    			
    			if (pixel < treshold)
    			{
    				area.addPoint(new Point(w, cy, pixel));
    			}
    			else
    			{
					break;
				}
    		}
    		
    		int e, ex = (int)current.x + 1; 
    		for (e = ex; e < width && !visited[e][cy]; e++)
    		{
    			pixel = getColor(image, e, cy);
    			visited[e][cy] = true;
    			
    			if (pixel < treshold)
    			{
    				area.addPoint(new Point(e, cy, pixel));
    			}
    			else
    			{
					break;
				}
    		}
    		
    		w = w < 0? 0 : w;
    		e = e == width? width - 1 : e;
    		
    		for (int xx = w; xx <= e; xx++)
    		{
    			if (cy > 0 && !visited[xx][cy-1])
    			{
    				visited[xx][cy-1] = true;
    				pixel = getColor(image, xx, cy - 1);
    				
    				if(pixel < treshold)
    				{
    					queue.add(new Point(xx, cy - 1, pixel));
    				}
    			}
    			
    			if (cy < height - 1 && !visited[xx][cy+1])
    			{
    				visited[xx][cy+1] = true;
    				pixel = getColor(image, xx, cy + 1);
    				
    				if (pixel < treshold)
    				{
    					queue.add(new Point(xx, cy + 1, pixel));
    				}
    			}    			
    		}
    	}
    	
    	return area;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void reset()
    {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected DataTableSpec[] configure(final DataTableSpec[] inSpecs)
            throws InvalidSettingsException
    {
        return new DataTableSpec[]{getSpec()};
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void saveSettingsTo(final NodeSettingsWO settings)
    {
    	this.settings.saveSettings(settings);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void loadValidatedSettingsFrom(final NodeSettingsRO settings)
            throws InvalidSettingsException
    {
    	this.settings.loadSettings(settings);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void validateSettings(final NodeSettingsRO settings)
            throws InvalidSettingsException
    {
    	this.settings.validateSettings(settings);
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    protected void loadInternals(final File internDir,
            final ExecutionMonitor exec) throws IOException,
            CanceledExecutionException
    {
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    protected void saveInternals(final File internDir,
            final ExecutionMonitor exec) throws IOException,
            CanceledExecutionException
    {
    }

}

