package org.knimelab.ocr.extractor;

import org.knime.core.node.NodeDialogPane;
import org.knime.core.node.NodeFactory;
import org.knime.core.node.NodeView;

/**
 * <code>NodeFactory</code> for the "FloodFillExtractor" Node.
 * 
 *
 * @author Team kNN
 */
public class FloodFillExtractorNodeFactory 
        extends NodeFactory<FloodFillExtractorNodeModel> {

    /**
     * {@inheritDoc}
     */
    @Override
    public FloodFillExtractorNodeModel createNodeModel() {
        return new FloodFillExtractorNodeModel();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getNrNodeViews() {
        return 0;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public NodeView<FloodFillExtractorNodeModel> createNodeView(final int viewIndex,
            final FloodFillExtractorNodeModel nodeModel) {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasDialog() {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public NodeDialogPane createNodeDialogPane() {
        return new FloodFillExtractorNodeDialog();
    }

}

