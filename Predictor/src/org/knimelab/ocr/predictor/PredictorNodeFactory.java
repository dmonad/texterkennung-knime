package org.knimelab.ocr.predictor;

import org.knime.core.node.NodeDialogPane;
import org.knime.core.node.NodeFactory;
import org.knime.core.node.NodeView;

/**
 * <code>NodeFactory</code> for the "Predictor" Node.
 * It compares images
 *
 * @author Kevin Jahns
 */
public class PredictorNodeFactory 
        extends NodeFactory<PredictorNodeModel> {

    /**
     * {@inheritDoc}
     */
    @Override
    public PredictorNodeModel createNodeModel() {
        return new PredictorNodeModel();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getNrNodeViews() {
        return 0;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public NodeView<PredictorNodeModel> createNodeView(final int viewIndex,
            final PredictorNodeModel nodeModel) {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasDialog() {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public NodeDialogPane createNodeDialogPane() {
        return new PredictorNodeDialog();
    }

}

