package org.knimelab.ocr;

import org.knime.core.data.DataColumnSpec;
import org.knime.core.node.BufferedDataTable;
import org.knime.core.node.InvalidSettingsException;
import org.knime.core.node.NodeSettingsRO;
import org.knime.core.node.NodeSettingsWO;
import org.knime.core.node.defaultnodesettings.SettingsModelString;

public class ImageNormalizerNodeSettings {

	public final SettingsModelString imageName =
    		new SettingsModelString("Learner image column", "Image");
	
	public String getImageName(){
		return imageName.getStringValue();
	}
	
	public DataColumnSpec getImageColumnSpec(BufferedDataTable table){
		return table.getDataTableSpec().getColumnSpec(getImageName());
	}
	
    public void saveSettings(final NodeSettingsWO settings)
    {
    	imageName.saveSettingsTo(settings);
    }

    public void loadSettings(final NodeSettingsRO settings)
            throws InvalidSettingsException
    {
    	imageName.loadSettingsFrom(settings);
    }

    public void validateSettings(final NodeSettingsRO settings)
            throws InvalidSettingsException
    {
    	imageName.validateSettings(settings);
    }
    
}
