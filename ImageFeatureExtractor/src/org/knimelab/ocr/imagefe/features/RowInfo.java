package org.knimelab.ocr.imagefe.features;

class RowInfo
{
	public int width;
	public int height;
	
	double cx, cy;
	double hDiff, vDiff;
}