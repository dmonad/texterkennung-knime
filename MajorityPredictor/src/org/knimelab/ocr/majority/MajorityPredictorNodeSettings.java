package org.knimelab.ocr.majority;

import java.util.List;

import org.knime.core.node.InvalidSettingsException;
import org.knime.core.node.NodeSettingsRO;
import org.knime.core.node.NodeSettingsWO;
import org.knime.core.node.defaultnodesettings.SettingsModelFilterString;

public class MajorityPredictorNodeSettings
{
	public final SettingsModelFilterString predictions = new SettingsModelFilterString("PredicitionColumns");
	
	public List<String> getIncludes()
	{
		return predictions.getIncludeList();
	}

    public void saveSettings(final NodeSettingsWO settings)
    {
    	predictions.saveSettingsTo(settings);       		
    }

    public void loadSettings(final NodeSettingsRO settings)
            throws InvalidSettingsException
    {
    	predictions.loadSettingsFrom(settings);
    }

    public void validateSettings(final NodeSettingsRO settings)
            throws InvalidSettingsException
    {
    	predictions.validateSettings(settings);
    }
}
