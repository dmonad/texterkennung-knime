package org.knimelab.ocr.extractor;

import org.knime.core.node.InvalidSettingsException;
import org.knime.core.node.NodeSettingsRO;
import org.knime.core.node.NodeSettingsWO;
import org.knime.core.node.defaultnodesettings.SettingsModelBoolean;
import org.knime.core.node.defaultnodesettings.SettingsModelInteger;

public class ExtractLettersNodeSettings {

	 SettingsModelInteger threshold = new SettingsModelInteger(
     		"Pixel color threshold for detection (0 (Black) - 255 (White)", 150);
     SettingsModelBoolean extraInfo = new SettingsModelBoolean("Attach some Information about the letter", false);
     
    public boolean getExtraInfo(){
    	return extraInfo.getBooleanValue();
    }
	 
	public int getThreshold(){
		return threshold.getIntValue();
	}
	
    public void saveSettings(final NodeSettingsWO settings)
    {
    	extraInfo.saveSettingsTo(settings);
    	threshold.saveSettingsTo(settings);
    }

    public void loadSettings(final NodeSettingsRO settings)
            throws InvalidSettingsException
    {
    	extraInfo.loadSettingsFrom(settings);
    	threshold.loadSettingsFrom(settings);
    }

    public void validateSettings(final NodeSettingsRO settings)
            throws InvalidSettingsException
    {
    	extraInfo.validateSettings(settings);
    	threshold.validateSettings(settings);
    }
}
