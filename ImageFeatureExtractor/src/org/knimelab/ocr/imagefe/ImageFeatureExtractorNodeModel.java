package org.knimelab.ocr.imagefe;

import java.io.File;
import java.io.IOException;

import org.knime.core.data.DataTableSpec;
import org.knime.core.data.container.ColumnRearranger;
import org.knime.core.node.BufferedDataTable;
import org.knime.core.node.CanceledExecutionException;
import org.knime.core.node.ExecutionContext;
import org.knime.core.node.ExecutionMonitor;
import org.knime.core.node.InvalidSettingsException;
import org.knime.core.node.NodeModel;
import org.knime.core.node.NodeSettingsRO;
import org.knime.core.node.NodeSettingsWO;

import org.knimelab.ocr.imagefe.features.AngularImageFeatureColumnRearranger;
import org.knimelab.ocr.imagefe.features.HorizontalImageFeatureColumnRearranger;
import org.knimelab.ocr.imagefe.features.RadialImageFeatureColumnRearranger;
import org.knimelab.ocr.imagefe.features.VerticalImageFeatureColumnRearranger;

/**
 * This is the model implementation of RadialFeatureExtractor.
 * 
 *
 * @author Team kNN
 */
public class ImageFeatureExtractorNodeModel extends NodeModel
{
    private ImageFeatureExtractorNodeSettings settings = new ImageFeatureExtractorNodeSettings();
    
    /**
     * Constructor for the node model.
     */
    protected ImageFeatureExtractorNodeModel()
    {
        super(1, 1);
    }
    
    protected ColumnRearranger getColumnRearranger(DataTableSpec original) throws InvalidSettingsException
    {
    	switch (settings.getMode())
    	{
			case "Radial":
				return new RadialImageFeatureColumnRearranger(original, settings);	
				
			case "Angular":
				return new AngularImageFeatureColumnRearranger(original, settings);
				
			case "Horizontal":
				return new HorizontalImageFeatureColumnRearranger(original, settings);
				
			case "Vertical":
				return new VerticalImageFeatureColumnRearranger(original, settings);
				
			default:
				throw new InvalidSettingsException("Invalid mode: " + settings.getMode());
		}
	}

    /**
     * {@inheritDoc}
     */
    @Override
    protected BufferedDataTable[] execute(final BufferedDataTable[] inData,
            final ExecutionContext exec) throws Exception
    {
    	ColumnRearranger cr = getColumnRearranger(inData[0].getDataTableSpec());
    	
    	BufferedDataTable table = exec.createColumnRearrangeTable(inData[0], cr, exec);
    	
        return new BufferedDataTable[]{table};
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void reset()
    {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected DataTableSpec[] configure(final DataTableSpec[] inSpecs)
            throws InvalidSettingsException
    {    	
    	DataTableSpec spec = getColumnRearranger(inSpecs[0]).createSpec();

        return new DataTableSpec[]{spec};
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void saveSettingsTo(final NodeSettingsWO settings)
    {
    	this.settings.saveSettings(settings);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void loadValidatedSettingsFrom(final NodeSettingsRO settings)
            throws InvalidSettingsException
    {
    	this.settings.loadSettings(settings);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void validateSettings(final NodeSettingsRO settings)
            throws InvalidSettingsException
    {
    	this.settings.validateSettings(settings);
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    protected void loadInternals(final File internDir,
            final ExecutionMonitor exec) throws IOException,
            CanceledExecutionException
    {
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    protected void saveInternals(final File internDir,
            final ExecutionMonitor exec) throws IOException,
            CanceledExecutionException
    {
    }

}

