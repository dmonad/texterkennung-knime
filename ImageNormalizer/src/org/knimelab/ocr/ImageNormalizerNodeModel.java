package org.knimelab.ocr;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.knime.core.data.DataCell;
import org.knime.core.data.DataRow;
import org.knime.core.data.DataTableSpec;
import org.knime.core.data.RowKey;
import org.knime.core.data.def.DefaultRow;
import org.knime.core.data.image.png.PNGImageContent;
import org.knime.core.data.image.png.PNGImageValue;
import org.knime.core.node.BufferedDataContainer;
import org.knime.core.node.BufferedDataTable;
import org.knime.core.node.CanceledExecutionException;
import org.knime.core.node.ExecutionContext;
import org.knime.core.node.ExecutionMonitor;
import org.knime.core.node.InvalidSettingsException;
import org.knime.core.node.NodeModel;
import org.knime.core.node.NodeSettingsRO;
import org.knime.core.node.NodeSettingsWO;


/**
 * This is the model implementation of ImageNormalizer.
 * 
 *
 * @author Kevin Jahns
 */
public class ImageNormalizerNodeModel extends NodeModel {
    
	private final ImageNormalizerNodeSettings settings = new ImageNormalizerNodeSettings();

    private static int tmp_enumerator_count = 0;
    private int tmp_enumerator; 

    synchronized void setTmpEnumerator(){
    	tmp_enumerator = tmp_enumerator_count++;    	
    }
	
    /**
     * Constructor for the node model.
     */
    protected ImageNormalizerNodeModel() {
    
        // TODO one incoming port and one outgoing port is assumed
        super(1, 1);
        
        setTmpEnumerator();
    }

    public DataTableSpec createSpec(BufferedDataTable table){
    	return table.getDataTableSpec();
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    protected BufferedDataTable[] execute(final BufferedDataTable[] inData,
            final ExecutionContext exec) throws Exception {

        // the data table spec of the single output table, 
        // the table will have three columns:        
        DataTableSpec outputSpec =  createSpec(inData[0]);
        String[] colNames = inData[0].getDataTableSpec().getColumnNames();
        
        // the execution context will provide us with storage capacity, in this
        // case a data container to which we will add rows sequentially
        // Note, this container can also handle arbitrary big data tables, it
        // will buffer to disc if necessary.
        BufferedDataContainer container = exec.createDataContainer(outputSpec);

        int imageCol = inData[0].getSpec().findColumnIndex(settings.getImageName());
        int i = 0;
        int i_end = inData[0].getRowCount();
        for (DataRow row : inData[0]){
        	
        	BufferedImage image = (BufferedImage) ((PNGImageValue) row.getCell(imageCol))
        			.getImageContent().getImage();
        	        	        	
        	// count average white
        	double average = 0;
        	for(int x = 0; x< image.getWidth(); x++){
        		for(int y = 0; y< image.getHeight(); y++){
        			average += new Color(image.getRGB(x, y)).getBlue();
        		}
        	}
        	// actually calc average black
        	average = average / (255*image.getWidth()*image.getHeight());
        	// normalize image
        	for(int x = 0; x< image.getWidth(); x++){
        		for(int y = 0; y< image.getHeight(); y++){
        			int color = new Color(image.getRGB(x, y)).getBlue();
        			if(color != 255){
        				int grey = (int) (255- (255-color) * average);
        				int newColor = grey + (grey << 8) + (grey << 16 );
        				image.setRGB(x, y, newColor);
        			}
        		}
        	}

        	image.flush();
        	
        	File tmp = File.createTempFile(tmp_enumerator + "knime_tmp_normalizer" + i, ".png");
        	ImageIO.write(image, "png", tmp);
	          
        	RowKey key = row.getKey();
        	DataCell[] cells = new DataCell[colNames.length];
        	for(int j = 0; j< colNames.length; j++){
        		if(j!=imageCol){
        			cells[j] = row.getCell(j);
        		} else {
        			cells[j] = (new PNGImageContent(new FileInputStream(tmp))).toImageCell(); 
        		}
        	}
        	
        	DataRow rowResult = new DefaultRow(key,cells);
        	container.addRowToTable(rowResult);
        
        	exec.checkCanceled();
            exec.setProgress(i / i_end,
                "Adding row " + i++);
        }
        
        // once we are done, we close the container and return its table
        container.close();
        BufferedDataTable out = container.getTable();
        return new BufferedDataTable[]{out};
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void reset() {
        // TODO Code executed on reset.
        // Models build during execute are cleared here.
        // Also data handled in load/saveInternals will be erased here.
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected DataTableSpec[] configure(final DataTableSpec[] inSpecs)
            throws InvalidSettingsException {
        
        // TODO: check if user settings are available, fit to the incoming
        // table structure, and the incoming types are feasible for the node
        // to execute. If the node can execute in its current state return
        // the spec of its output data table(s) (if you can, otherwise an array
        // with null elements), or throw an exception with a useful user message

        return new DataTableSpec[]{null};
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void saveSettingsTo(final NodeSettingsWO settings)
    {
        this.settings.saveSettings(settings);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void loadValidatedSettingsFrom(final NodeSettingsRO settings)
            throws InvalidSettingsException
    {
        this.settings.loadSettings(settings);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void validateSettings(final NodeSettingsRO settings)
            throws InvalidSettingsException
    {
        this.settings.validateSettings(settings);
    }
    
    
    /**
     * {@inheritDoc}
     */
    @Override
    protected void loadInternals(final File internDir,
            final ExecutionMonitor exec) throws IOException,
            CanceledExecutionException {
        
        // TODO load internal data. 
        // Everything handed to output ports is loaded automatically (data
        // returned by the execute method, models loaded in loadModelContent,
        // and user settings set through loadSettingsFrom - is all taken care 
        // of). Load here only the other internals that need to be restored
        // (e.g. data used by the views).

    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    protected void saveInternals(final File internDir,
            final ExecutionMonitor exec) throws IOException,
            CanceledExecutionException {
       
        // TODO save internal models. 
        // Everything written to output ports is saved automatically (data
        // returned by the execute method, models saved in the saveModelContent,
        // and user settings saved through saveSettingsTo - is all taken care 
        // of). Save here only the other internals that need to be preserved
        // (e.g. data used by the views).

    }

}

