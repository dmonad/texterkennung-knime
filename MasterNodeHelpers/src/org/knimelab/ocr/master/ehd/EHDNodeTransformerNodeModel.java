package org.knimelab.ocr.master.ehd;

import java.io.File;
import java.io.IOException;

import org.knime.core.data.DataCell;
import org.knime.core.data.DataColumnSpec;
import org.knime.core.data.DataColumnSpecCreator;
import org.knime.core.data.DataRow;
import org.knime.core.data.DataTableSpec;
import org.knime.core.data.container.AbstractCellFactory;
import org.knime.core.data.container.ColumnRearranger;
import org.knime.core.data.def.DoubleCell;
import org.knime.core.data.def.StringCell;
import org.knime.core.node.BufferedDataTable;
import org.knime.core.node.CanceledExecutionException;
import org.knime.core.node.ExecutionContext;
import org.knime.core.node.ExecutionMonitor;
import org.knime.core.node.InvalidSettingsException;
import org.knime.core.node.NodeModel;
import org.knime.core.node.NodeSettingsRO;
import org.knime.core.node.NodeSettingsWO;


/**
 * This is the model implementation of EHDNodeTransformer.
 * 
 *
 * @author Team kNN
 */
public class EHDNodeTransformerNodeModel extends NodeModel
{
	public final double NaN_Value = 0;
	
    /**
     * Constructor for the node model.
     */
    protected EHDNodeTransformerNodeModel()
    {    
        super(1, 1);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected BufferedDataTable[] execute(final BufferedDataTable[] inData,
            final ExecutionContext exec) throws Exception
    {
    	final int index = inData[0].getDataTableSpec().findColumnIndex("EHD");
    	final DataRow row = inData[0].iterator().next();
    	
    	final String ehdString = ((StringCell)row.getCell(index)).getStringValue();
    	
    	final double[][][] data = EhdSerializer.deserialize(ehdString);
    	
    	final int width = data.length;
    	final int height = data[0].length;
    	final int bins = data[0][0].length;
    	final int total = width * height * bins;
    	
    	final DataColumnSpec[] specs = new DataColumnSpec[total];
    	
    	int i = 0;
    	for (int x = 0; x < width; x++)
    	{
        	for (int y = 0; y < height; y++)
        	{
            	for (int b = 0; b < bins; b++)
            	{
            		specs[i++] = new DataColumnSpecCreator("Cell " + x + "," + y + " Bin " + i, DoubleCell.TYPE).createSpec();            		
            	}
        	}        	
		} 
    	
    	ColumnRearranger columnRearranger = new ColumnRearranger(inData[0].getDataTableSpec());
    	
    	columnRearranger.append(new AbstractCellFactory(true, specs)
    	{			
			@Override
			public DataCell[] getCells(DataRow row)
			{		    	
		    	final String ehdString = ((StringCell)row.getCell(index)).getStringValue();		    	
		    	final double[][][] data = EhdSerializer.deserialize(ehdString);
		    	
		        final DoubleCell[] cells = new DoubleCell[total];
		    	
		    	int i = 0;
		    	for (int x = 0; x < width; x++)
		    	{
		        	for (int y = 0; y < height; y++)
		        	{
		            	for (int b = 0; b < bins; b++)
		            	{
		            		cells[i++] = new DoubleCell(Double.isNaN(data[x][y][b])? NaN_Value : data[x][y][b]);		            		
		            	}
		        	}        	
				} 
		    	
		    	return cells;
			}
		});
    	
    	BufferedDataTable table = exec.createColumnRearrangeTable(inData[0], columnRearranger, exec);
    	
        return new BufferedDataTable[]{ table };
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void reset()
    {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected DataTableSpec[] configure(final DataTableSpec[] inSpecs)
            throws InvalidSettingsException
    {
        return new DataTableSpec[]{null};
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void saveSettingsTo(final NodeSettingsWO settings)
    {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void loadValidatedSettingsFrom(final NodeSettingsRO settings)
            throws InvalidSettingsException
    {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void validateSettings(final NodeSettingsRO settings)
            throws InvalidSettingsException
    {
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    protected void loadInternals(final File internDir,
            final ExecutionMonitor exec) throws IOException,
            CanceledExecutionException
    {
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    protected void saveInternals(final File internDir,
            final ExecutionMonitor exec) throws IOException,
            CanceledExecutionException
    {
    }
}

