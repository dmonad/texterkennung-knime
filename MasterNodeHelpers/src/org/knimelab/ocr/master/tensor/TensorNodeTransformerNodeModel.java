package org.knimelab.ocr.master.tensor;

import java.io.File;
import java.io.IOException;

import org.knime.core.data.DataCell;
import org.knime.core.data.DataColumnSpec;
import org.knime.core.data.DataColumnSpecCreator;
import org.knime.core.data.DataRow;
import org.knime.core.data.DataTableSpec;
import org.knime.core.data.container.AbstractCellFactory;
import org.knime.core.data.container.ColumnRearranger;
import org.knime.core.data.def.DoubleCell;
import org.knime.core.node.BufferedDataTable;
import org.knime.core.node.CanceledExecutionException;
import org.knime.core.node.ExecutionContext;
import org.knime.core.node.ExecutionMonitor;
import org.knime.core.node.InvalidSettingsException;
//import org.knime.core.node.NodeLogger;
import org.knime.core.node.NodeModel;
import org.knime.core.node.NodeSettingsRO;
import org.knime.core.node.NodeSettingsWO;


/**
 * This is the model implementation of TensorNodeTransformer.
 * 
 *
 * @author KNN
 */
public class TensorNodeTransformerNodeModel extends NodeModel {
    
    /* the logger instance
    private static final NodeLogger logger = NodeLogger
            .getLogger(TensorNodeTransformerNodeModel.class);*/

    /**
     * Constructor for the node model.
     */
    protected TensorNodeTransformerNodeModel() {
        super(1, 1);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected BufferedDataTable[] execute(final BufferedDataTable[] inData,
            final ExecutionContext exec) throws Exception {

        /* TODO do something here
        logger.info("Node Model Stub... this is not yet implemented !");*/

        String tensor = inData[0].iterator().next().getCell(1).toString();
        String[] tensors = new String[2];
        tensors = tensor.split("\\|");
        String[] size = new String[2];
        size = tensors[0].split("#");
        size = size[0].split(",");
        final int tensorCount = Integer.parseInt(size[0]) * Integer.parseInt(size[1]);

        final DataColumnSpec[] specs = new DataColumnSpec[4 * tensorCount];
        for (int specCount = 0; specCount < 4 * tensorCount; ++ specCount)
    		specs[specCount] = new DataColumnSpecCreator("Cell " + specCount, DoubleCell.TYPE).createSpec();

        ColumnRearranger columnRearranger = new ColumnRearranger(inData[0].getDataTableSpec());
    	columnRearranger.append(new AbstractCellFactory(true, specs)
    	{
    		public DataCell[] getCells(DataRow row)
    		{		    	
    			final DoubleCell[] cells = new DoubleCell[4 * tensorCount];
    			  	String tensor = row.getCell(1).toString();
    		        String[] tensors = new String[2];
    		        tensors = tensor.split("\\|");
    		        String[] size = new String[2];
    		        size = tensors[0].split("#");
    		        size = size[0].split(",");
    		        String[] matrix = new String[tensorCount];
    	        	matrix = tensors[1].split("#");

    				for (int i = 0, k = 0; i < tensorCount; ++ i) {

    		        	String[] stringRow = matrix[i].split("\\; ");
    		        	String[] left = stringRow[0].split("\\, ");
    		        	String[] right = stringRow[1].split("\\, ");
    		        	
    		        	if ("NaN".equals(left[0]))
    		        		left[0] = "0";
    		        	if ("NaN".equals(left[1]))
    		        		left[1] = "0";
    		        	if ("NaN".equals(right[0]))
    		        		right[0] = "0";
    		        	if ("NaN".equals(right[1]))
    		        		right[1] = "0";

    		        	cells[k ++] = new DoubleCell(Double.parseDouble(left[0].replace(',', '.')));
    		        	cells[k ++] = new DoubleCell(Double.parseDouble(left[1].replace(',', '.')));
    		        	cells[k ++] = new DoubleCell(Double.parseDouble(right[0].replace(',', '.')));
    		        	cells[k ++] = new DoubleCell(Double.parseDouble(right[1].replace(',', '.')));

    			}
    			return cells;
    		}
    	});

    	BufferedDataTable table = exec.createColumnRearrangeTable(inData[0], columnRearranger, exec);
    	return new BufferedDataTable[]{ table };
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void reset() {
        ;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected DataTableSpec[] configure(final DataTableSpec[] inSpecs)
            throws InvalidSettingsException {
        return new DataTableSpec[]{null};
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void saveSettingsTo(final NodeSettingsWO settings) {
        ;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void loadValidatedSettingsFrom(final NodeSettingsRO settings)
            throws InvalidSettingsException {
        ;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void validateSettings(final NodeSettingsRO settings)
            throws InvalidSettingsException {
        ;
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    protected void loadInternals(final File internDir,
            final ExecutionMonitor exec) throws IOException,
            CanceledExecutionException {
        ;
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    protected void saveInternals(final File internDir,
            final ExecutionMonitor exec) throws IOException,
            CanceledExecutionException {
        ;
    }

}

