package org.knimelab.ocr.stringextractor;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.PriorityQueue;

import org.knime.core.data.DataCell;
import org.knime.core.data.DataColumnSpec;
import org.knime.core.data.DataColumnSpecCreator;
import org.knime.core.data.DataRow;
import org.knime.core.data.DataTableSpec;
import org.knime.core.data.def.DefaultRow;
import org.knime.core.data.def.IntCell;
import org.knime.core.data.def.StringCell;
import org.knime.core.data.image.png.PNGImageValue;
import org.knime.core.node.BufferedDataContainer;
import org.knime.core.node.BufferedDataTable;
import org.knime.core.node.CanceledExecutionException;
import org.knime.core.node.ExecutionContext;
import org.knime.core.node.ExecutionMonitor;
import org.knime.core.node.InvalidSettingsException;
import org.knime.core.node.NodeModel;
import org.knime.core.node.NodeSettingsRO;
import org.knime.core.node.NodeSettingsWO;
import org.knime.knip.base.data.img.ImgPlusCell;

class Letter
{
	public final int x;
	public final int y;
	public final int width;
	public final int height;
	public final double cx;
	public final double cy;
	public final String letter;
	
	public Letter(int x, int y, String letter, int w, int h)
	{
		this.x = x;
		this.y = y;
		this.letter = letter;
		this.width = w;
		this.height = h;
		
		if (letter == "." || letter == ",")
		{
			this.cx = x;
			this.cy = y;
		}
		else
		{
			this.cx = x + width * 0.5;
			this.cy = y + height * 0.5;
		}
	}
}
class Line
{
	public final int minX;
	public final int minY;
	public final int maxX;
	public final int maxY;
	public final int width;
	public final int height;
	public final int totalLetterWidth;
	public final int count;
	public final PriorityQueue<Letter> letters;
	
	public Line(Collection<Letter> letters)
	{
		this.letters = new PriorityQueue<Letter>(		
			letters.size(),
			new Comparator<Letter>() {
				@Override
				public int compare(Letter o1, Letter o2) {
					return Integer.compare(o1.x, o2.x);
				}
			}
		);
		
		int c = 0;	
		
		this.letters.addAll(letters);		
		
		int minX = Integer.MAX_VALUE, maxX = 0;
		int minY = Integer.MAX_VALUE, maxY = 0;
		int totalWidth = 0;
		
		for (Letter letter : letters)
		{
			if (!letter.letter.equals(".") && !letter.letter.equals(","))
			{
				c++;
				totalWidth += letter.width;
			}
			
			if (letter.x < minX) minX = letter.x;
			if (letter.x + letter.width > maxX) maxX = letter.x + letter.width;
			if (letter.y < minY) minY = letter.y;
			if (letter.y + letter.height > maxY) maxY = letter.y + letter.height;
		}

		this.minX = minX;
		this.maxX = maxX;
		this.minY = minY;
		this.maxY = maxY;

		width = maxX - minX;
		height = maxY - minY;
		
		totalLetterWidth = totalWidth;
		count = c;
	}
}

/**
 * This is the model implementation of StringExtractor.
 * 
 * @author Team kNN
 */
public class StringExtractorNodeModel extends NodeModel
{
    public static final float FUZZYNESS = 0.1f;
    
    public static final HashMap<String, String> PAIRS = new HashMap<String, String>(4);
    
    static
    {
    	PAIRS.put("0", "O");
    	PAIRS.put("1", "I");
    	PAIRS.put("2", "Z");
    	PAIRS.put("8", "B");    	
    }
	
	
    /**
     * Constructor for the node model.
     */
    protected StringExtractorNodeModel() {
        super(1, 1);
    }
    
    private DataTableSpec getSpec() {
        DataColumnSpec[] allColSpecs = new DataColumnSpec[] {
        	// new DataColumnSpecCreator("String", StringCell.TYPE).createSpec()
        		new DataColumnSpecCreator("MinX", IntCell.TYPE).createSpec(),
        		new DataColumnSpecCreator("MinY", IntCell.TYPE).createSpec(),
        		new DataColumnSpecCreator("MaxX", IntCell.TYPE).createSpec(),
        		new DataColumnSpecCreator("MaxY", IntCell.TYPE).createSpec(),
        		new DataColumnSpecCreator("String", StringCell.TYPE).createSpec()
        };
        
        return new DataTableSpec(allColSpecs);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected BufferedDataTable[] execute(final BufferedDataTable[] inData,
            final ExecutionContext exec) throws Exception {        
        // the execution context will provide us with storage capacity, in this
        // case a data container to which we will add rows sequentially
        // Note, this container can also handle arbitrary big data tables, it
        // will buffer to disc if necessary.
        BufferedDataContainer container = exec.createDataContainer(getSpec());
        
        int xIndex = inData[0].getDataTableSpec().findColumnIndex("X");
        int yIndex = inData[0].getDataTableSpec().findColumnIndex("Y");
        int letterIndex = inData[0].getDataTableSpec().findColumnIndex("Letter");
        int imageIndex = inData[0].getDataTableSpec().findColumnIndex("Image");
        
        Letter[] letters = new Letter[inData[0].getRowCount()];
        
        int i = 0;
        for (DataRow row : inData[0]) {
        	int x = ((IntCell)row.getCell(xIndex)).getIntValue();
        	int y = ((IntCell)row.getCell(yIndex)).getIntValue();
        	String letter = ((StringCell)row.getCell(letterIndex)).getStringValue();
        	
        	DataCell cell = row.getCell(imageIndex);
        	
        	if (cell instanceof ImgPlusCell<?>)
        	{
        		long[] dimensions = ((ImgPlusCell<?>) cell).getDimensions(); 
    			letters[i++] = new Letter(x, y, letter, (int)dimensions[0], (int)dimensions[1]);		       		
        	}
        	else
        	{
        		BufferedImage img = (BufferedImage) ((PNGImageValue) cell).getImageContent().getImage();
    			letters[i++] = new Letter(x, y, letter, img.getWidth(), img.getHeight());				
			}
        	
		}
        
        PriorityQueue<Line> lines = new PriorityQueue<Line>(1, new Comparator<Line>() {
			@Override
			public int compare(Line o1, Line o2) {
				return Integer.compare(o1.minY, o2.minY);
			}
		});
        
        int lastIndex = letters.length;
        for (i = 0; i < lastIndex; i++)
        {
            float minY = letters[i].y + FUZZYNESS * letters[i].height;
            float maxY = letters[i].y + (1 - FUZZYNESS) * letters[i].height;
            
            ArrayList<Letter> line_letters = new ArrayList<Letter>();
            
        	line_letters.add(letters[i]);
            
            for(int j=i+1; j < lastIndex;)
            {
                float lowY  = letters[j].y + FUZZYNESS * letters[j].height;
                float highY = letters[j].y + (1 - FUZZYNESS) * letters[j].height;
                
                if (lowY <= maxY && highY >= minY)
                {
                	line_letters.add(letters[j]);
                	letters[j] = letters[--lastIndex];
                	
                	minY = lowY < minY? lowY : minY;
                	maxY = highY > maxY? highY : maxY;
                }
                else {
					j++;
				}
            }
            
            Line l = new Line(line_letters);
            
            lines.add(l);
		}
        
        StringBuilder sb = new StringBuilder();

        int ll = 0;
        int lastY = 0;
		while(!lines.isEmpty())
		{
			Line line = lines.poll();
			int lc = (line.minY - lastY - line.height / 2) / line.height;
			while (lc-- > 0)
			{
				sb.append('\n');
			}
			lastY = line.maxY;
			
			double avgWidth = line.totalLetterWidth / (double)(line.count);
			
			int lastX = letters[0].x;
			StringBuilder lsb = new StringBuilder();
			while(!line.letters.isEmpty())
			{
				Letter letter = line.letters.poll();
				lc = (int) ((letter.x - lastX) / (avgWidth * 0.5));
				while (lc-- > 0)
				{
					lsb.append(' ');
				}
				lastX = letter.x + letter.width;
				
				lsb.append(letter.letter);
			}
			
			String stringLine = lsb.toString();
			
			for (Entry<String, String> pair : PAIRS.entrySet())
			{
				stringLine = stringLine.replaceAll("(?<=\\d+|^|\\s+)"+pair.getValue()+"(?=\\W+|$^|\\s+)", pair.getKey());
				stringLine = stringLine.replaceAll("(?<=\\W+|^|\\s+)"+pair.getValue()+"(?=\\d+|$^|\\s+)", pair.getKey());
				stringLine = stringLine.replaceAll("(?<=\\w+|^|\\s+)"+pair.getKey()+"(?=\\D+|$^|\\s+)", pair.getValue());
				stringLine = stringLine.replaceAll("(?<=\\D+|^|\\s+)"+pair.getKey()+"(?=\\w+|$^|\\s+)", pair.getValue());				
			}			
			
			container.addRowToTable(new DefaultRow(
					"Line " + ++ll,
					new IntCell(line.minX),
					new IntCell(line.minY),
					new IntCell(line.maxX),
					new IntCell(line.maxY),
					new StringCell(stringLine)
			));
			
			sb.append(stringLine);
		}

		container.addRowToTable(new DefaultRow(
				"Total",
				new IntCell(0),
				new IntCell(0),
				new IntCell(0),
				new IntCell(0),
				new StringCell(sb.toString())
		));
		
        container.close();
        
        return new BufferedDataTable[]{container.getTable()};
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void reset() {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected DataTableSpec[] configure(final DataTableSpec[] inSpecs)
            throws InvalidSettingsException {
        return new DataTableSpec[]{getSpec()};
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void saveSettingsTo(final NodeSettingsWO settings) {
         // TODO: generated method stub
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void loadValidatedSettingsFrom(final NodeSettingsRO settings)
            throws InvalidSettingsException {
        // TODO: generated method stub
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void validateSettings(final NodeSettingsRO settings)
            throws InvalidSettingsException {
        // TODO: generated method stub
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    protected void loadInternals(final File internDir,
            final ExecutionMonitor exec) throws IOException,
            CanceledExecutionException {
        // TODO: generated method stub
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    protected void saveInternals(final File internDir,
            final ExecutionMonitor exec) throws IOException,
            CanceledExecutionException {
        // TODO: generated method stub
    }

}

