package org.knimelab.ocr.imagefe.features;

import java.awt.Color;
import java.awt.Graphics;

import org.knime.core.data.DataTableSpec;
import org.knimelab.ocr.imagefe.ImageFeatureExtractorNodeSettings;

public class AngularImageFeatureColumnRearranger extends ImageFeatureColumnRearranger<RowInfo>
{
	public AngularImageFeatureColumnRearranger(DataTableSpec original,
			ImageFeatureExtractorNodeSettings settings)
	{
		super(original, settings, "Angular");
	}

	@Override
	protected void setup(RowInfo data)
	{		
	}	
	
	@Override
	protected void drawDebug(Graphics g, RowInfo data)
	{
		double radius = Math.sqrt(data.hDiff * data.hDiff + data.vDiff * data.vDiff);
		
		g.setColor(Color.RED);		
		g.fillRect((int)data.cx-1, (int)data.cy-1, 2, 2);
		
		for (double a = -Math.PI; a < Math.PI; a += 2*Math.PI/n)
		{
			int x = (int) (data.cx + Math.sin(a) * radius);
			int y = (int) (data.cy + Math.cos(a) * radius);
			
			g.drawLine((int)data.cx, (int)data.cy, x, y);
			g.drawLine(x, y, (int)data.cx, (int)data.cy);
		}
	}

	@Override
	protected int getBin(int x, int y, double f, RowInfo data)
	{
		double angle = Math.atan2(x - data.cx, y - data.cy);
		double d = (angle + Math.PI) / 2 / Math.PI * n;
		
		return (int)d;
	}
}