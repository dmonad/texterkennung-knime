package knimelab.ocr;

import org.knime.core.node.defaultnodesettings.DefaultNodeSettingsPane;

/**
 * <code>NodeDialog</code> for the "MuPredictor" Node.
 * 
 *
 * This node dialog derives from {@link DefaultNodeSettingsPane} which allows
 * creation of a simple dialog with standard components. If you need a more 
 * complex dialog please derive directly from 
 * {@link org.knime.core.node.NodeDialogPane}.
 * 
 * @author kNN
 */
public class MuPredictorNodeDialog extends DefaultNodeSettingsPane {

    /**
     * New pane for configuring the MuPredictor node.
     */
    protected MuPredictorNodeDialog() {

    }
}

