package org.knimelab.ocr.master.ehd;

import org.knime.core.node.NodeDialogPane;
import org.knime.core.node.NodeFactory;
import org.knime.core.node.NodeView;

/**
 * <code>NodeFactory</code> for the "EHDNodeTransformer" Node.
 * 
 *
 * @author Team kNN
 */
public class EHDNodeTransformerNodeFactory 
        extends NodeFactory<EHDNodeTransformerNodeModel> {

    /**
     * {@inheritDoc}
     */
    @Override
    public EHDNodeTransformerNodeModel createNodeModel() {
        return new EHDNodeTransformerNodeModel();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getNrNodeViews() {
        return 0;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public NodeView<EHDNodeTransformerNodeModel> createNodeView(final int viewIndex,
            final EHDNodeTransformerNodeModel nodeModel) {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasDialog() {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public NodeDialogPane createNodeDialogPane() {
        return new EHDNodeTransformerNodeDialog();
    }

}

