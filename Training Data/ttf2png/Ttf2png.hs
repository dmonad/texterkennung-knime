import qualified Graphics.UI.SDL.General as SDLg
import qualified Graphics.UI.SDL.TTF.General as SDLttf

import Graphics.UI.SDL.TTF.Management
import Graphics.UI.SDL.TTF.Render
import Graphics.UI.SDL.Color
import Graphics.UI.SDL.Video
import Graphics.UI.SDL.TTF.Types
import Codec.Image.DevIL

import System.FilePath.Posix
import System.Directory
import System.Environment
import Control.Monad
import Control.Applicative

import Data.List


main ::  IO ()
main = do
  print "In case you get an error, try again. This is a bug of massive parallelism in Haskell/the library I am using (to many open file handlers)"

  SDLg.init [SDLg.InitEverything]
  SDLttf.init
  ilInit

  args <- getArgs
  when (null args) $ error "Need input Dir/File of .ttf files.. \n Example: ttf2png /usr/share/fonts" 
  let ttfDir = head args
      trainingdir = 
        if   length args < 2 
        then "Training Data"
        else args!!1

  trainingDirExists <- doesDirectoryExist trainingdir
  unless trainingDirExists $ createDirectory trainingdir

  ttfFiles <- filter (isSuffixOf ".ttf") <$> recursiveListFiles ttfDir
  mapM_ (ttf2Letters trainingdir) ttfFiles

  bmpFiles <- filter (isSuffixOf ".bmp") <$> recursiveListFiles trainingdir
  mapM_ bmp2png bmpFiles

  SDLg.quit 
  print $ "See your pretty new fonts in '" ++ trainingdir ++ "'"

bmp2png ::  [Char] -> IO ()
bmp2png fileBmp = do 
  let filePng = addExtension (dropExtension fileBmp) ".png"
  image <-readImage fileBmp
  writeImage filePng image
  removeFile fileBmp

recursiveListFiles ::  FilePath -> IO [FilePath]
recursiveListFiles path = do 
  isDir <- doesDirectoryExist path 
  case isDir of
       True -> do
          filepaths <- map (combine path) 
              <$> filter (\x->x/="." && x/="..")
              <$> getDirectoryContents path
          concat <$> mapM recursiveListFiles filepaths
       False -> return [path]


ttf2Letters ::  FilePath -> FilePath -> IO ()
ttf2Letters trainingDir ttfFile = do
    let newDir = combine trainingDir $ dropExtension $ takeFileName ttfFile
    newDirExists <- doesDirectoryExist newDir 
    unless newDirExists $ do 
      createDirectory newDir 
      font <- openFont ttfFile 24
      mapM_ (writeLetterToDirAsBMP font newDir) "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"

       
writeLetterToDirAsBMP :: Font -> FilePath -> Char -> IO Bool
writeLetterToDirAsBMP font dir char = do 
    let fileBmp = dir </> (char:".bmp")

    surface <- renderTextSolid font [char] (Color 0 0 0)
    saveBMP surface fileBmp





  

