package org.knimelab.ocr;

import org.knime.core.data.DataTableSpec;
import org.knime.core.data.image.png.PNGImageValue;
import org.knime.core.node.InvalidSettingsException;
import org.knime.core.node.NodeSettingsRO;
import org.knime.core.node.NotConfigurableException;
import org.knime.core.node.defaultnodesettings.DefaultNodeSettingsPane;
import org.knime.core.node.defaultnodesettings.DialogComponentColumnNameSelection;
import org.knime.core.node.util.ColumnFilter;
import org.knime.core.node.util.DataValueColumnFilter;

/**
 * <code>NodeDialog</code> for the "ImageNormalizer" Node.
 * 
 *
 * This node dialog derives from {@link DefaultNodeSettingsPane} which allows
 * creation of a simple dialog with standard components. If you need a more 
 * complex dialog please derive directly from 
 * {@link org.knime.core.node.NodeDialogPane}.
 * 
 * @author Kevin Jahns
 */
public class ImageNormalizerNodeDialog extends DefaultNodeSettingsPane {

	ImageNormalizerNodeSettings settings = new ImageNormalizerNodeSettings();
	
    /**
     * New pane for configuring ImageNormalizer node dialog.
     * This is just a suggestion to demonstrate possible default dialog
     * components.
     */
    @SuppressWarnings("unchecked")
	protected ImageNormalizerNodeDialog() {
        super();
        
        this.createNewGroup("Select column to normalize");
        DataValueColumnFilter filter = new DataValueColumnFilter(PNGImageValue.class);
        this.addDialogComponent(
        		new DialogComponentColumnNameSelection(settings.imageName, "Image column", 
        				0, (ColumnFilter) filter ));
                    
    }
    
    
    @Override
    protected void loadSettingsFrom(final NodeSettingsRO settings,
            final DataTableSpec[] specs) throws NotConfigurableException
    {
        try
        {
            this.settings.loadSettings(settings);
        }
        catch (InvalidSettingsException ex)
        {
            // Ignore errors and use the defaults
        }
    }
}

