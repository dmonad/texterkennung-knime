package org.knimelab.ocr.imagefe.features;

import java.awt.Graphics;

import org.knime.core.data.DataTableSpec;
import org.knimelab.ocr.imagefe.ImageFeatureExtractorNodeSettings;


class VerticalRowInfo extends RowInfo
{
	double h;
}

public class VerticalImageFeatureColumnRearranger extends
		ImageFeatureColumnRearranger<VerticalRowInfo>
{
	public VerticalImageFeatureColumnRearranger(DataTableSpec original,
			ImageFeatureExtractorNodeSettings settings)
	{
		super(original, settings, "Vertical");
	}

	@Override
	protected void setup(VerticalRowInfo data)
	{
		data.h = ((double)data.height) / n;
	}

	@Override
	protected void drawDebug(Graphics g, VerticalRowInfo data)
	{
		for (double y = data.h; y < data.height; y += data.h)
		{
			g.drawLine(0, (int)y, data.width, (int)y);
		}
	}

	@Override
	protected int getBin(int x, int y, double f, VerticalRowInfo data)
	{
		return (int) (y / data.h);
	}
}
