package knimelab.ocr;

import java.io.File;
import java.io.IOException;

import org.knime.core.data.DataCell;
import org.knime.core.data.DataColumnSpec;
import org.knime.core.data.DataColumnSpecCreator;
import org.knime.core.data.DataRow;
import org.knime.core.data.DataTableSpec;
import org.knime.core.data.collection.ListCell;
import org.knime.core.data.def.BooleanCell;
import org.knime.core.data.def.DefaultRow;
import org.knime.core.data.def.IntCell;
import org.knime.core.node.BufferedDataContainer;
import org.knime.core.node.BufferedDataTable;
import org.knime.core.node.CanceledExecutionException;
import org.knime.core.node.ExecutionContext;
import org.knime.core.node.ExecutionMonitor;
import org.knime.core.node.InvalidSettingsException;
import org.knime.core.node.NodeModel;
import org.knime.core.node.NodeSettingsRO;
import org.knime.core.node.NodeSettingsWO;


/**
 * This is the model implementation of PixelEnumerator.
 * 
 *
 * @author 
 */
public class PixelEnumeratorNodeModel extends NodeModel
{
    protected PixelEnumeratorNodeModel() {
    
        // TODO one incoming port and one outgoing port is assumed
        super(1, 1);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected BufferedDataTable[] execute(final BufferedDataTable[] inData,
            final ExecutionContext exec) throws Exception {    
        DataColumnSpec[] allColSpecs = new DataColumnSpec[]
        		{
        		 new DataColumnSpecCreator("x", IntCell.TYPE).createSpec(),
        		 new DataColumnSpecCreator("y", IntCell.TYPE).createSpec(),
        		 new DataColumnSpecCreator("pixel", IntCell.TYPE).createSpec(),
        		 new DataColumnSpecCreator("black", BooleanCell.TYPE).createSpec()
        		};
        
        DataTableSpec outputSpec = new DataTableSpec(allColSpecs);

        BufferedDataContainer container = exec.createDataContainer(outputSpec);


        DataRow row = inData[0].iterator().next();
        
        ListCell gsCell = (ListCell) row.getCell(inData[0].getDataTableSpec().findColumnIndex("GS"));
        ListCell swCell = (ListCell) row.getCell(inData[0].getDataTableSpec().findColumnIndex("SW"));
        int width = ((IntCell) row.getCell(inData[0].getDataTableSpec().findColumnIndex("Width"))).getIntValue();
        int height = ((IntCell) row.getCell(inData[0].getDataTableSpec().findColumnIndex("Height"))).getIntValue();
        int count =  gsCell.size();
        
        for (int i = 0; i < count; i++)
        {
            container.addRowToTable(
	        	new DefaultRow("Row " + i, new DataCell[]
	            {
	            	new IntCell(i / height),
	            	new IntCell(i % height),
	            	new IntCell(((IntCell)gsCell.get(i)).getIntValue()),
	            	BooleanCell.get(((BooleanCell)swCell.get(i)).getBooleanValue())
	            })
        	);

            exec.checkCanceled();
            exec.setProgress(i / count, "Adding row " + i);
        }

        container.close();
        BufferedDataTable out = container.getTable();
        
        return new BufferedDataTable[]{out};
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void reset() {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected DataTableSpec[] configure(final DataTableSpec[] inSpecs)
            throws InvalidSettingsException {
    	return new DataTableSpec[]{null};
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void saveSettingsTo(final NodeSettingsWO settings) {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void loadValidatedSettingsFrom(final NodeSettingsRO settings)
            throws InvalidSettingsException {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void validateSettings(final NodeSettingsRO settings)
            throws InvalidSettingsException {

    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    protected void loadInternals(final File internDir,
            final ExecutionMonitor exec) throws IOException,
            CanceledExecutionException {
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    protected void saveInternals(final File internDir,
            final ExecutionMonitor exec) throws IOException,
            CanceledExecutionException {
    }
}

