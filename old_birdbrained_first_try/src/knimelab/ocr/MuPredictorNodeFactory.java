package knimelab.ocr;

import org.knime.core.node.NodeDialogPane;
import org.knime.core.node.NodeFactory;
import org.knime.core.node.NodeView;

/**
 * <code>NodeFactory</code> for the "MuPredictor" Node.
 * 
 *
 * @author kNN
 */
public class MuPredictorNodeFactory 
        extends NodeFactory<MuPredictorNodeModel> {

    /**
     * {@inheritDoc}
     */
    @Override
    public MuPredictorNodeModel createNodeModel() {
        return new MuPredictorNodeModel();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getNrNodeViews() {
        return 0;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public NodeView<MuPredictorNodeModel> createNodeView(final int viewIndex,
            final MuPredictorNodeModel nodeModel) {
    	return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasDialog() {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public NodeDialogPane createNodeDialogPane() {
        return new MuPredictorNodeDialog();
    }

}

