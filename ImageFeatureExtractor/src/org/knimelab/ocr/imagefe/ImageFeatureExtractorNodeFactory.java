package org.knimelab.ocr.imagefe;

import org.knime.core.node.NodeDialogPane;
import org.knime.core.node.NodeFactory;
import org.knime.core.node.NodeView;

/**
 * <code>NodeFactory</code> for the "RadialFeatureExtractor" Node.
 * 
 *
 * @author Team kNN
 */
public class ImageFeatureExtractorNodeFactory 
        extends NodeFactory<ImageFeatureExtractorNodeModel>
{

    /**
     * {@inheritDoc}
     */
    @Override
    public ImageFeatureExtractorNodeModel createNodeModel()
    {
        return new ImageFeatureExtractorNodeModel();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getNrNodeViews()
    {
        return 0;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public NodeView<ImageFeatureExtractorNodeModel> createNodeView(final int viewIndex,
            final ImageFeatureExtractorNodeModel nodeModel)
    {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasDialog()
    {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public NodeDialogPane createNodeDialogPane()
    {
        return new ImageFeatureExtractorNodeDialog();
    }

}

