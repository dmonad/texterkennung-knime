package knimelab.ocr;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map.Entry;

import org.knime.core.data.DataCell;
import org.knime.core.data.DataColumnSpec;
import org.knime.core.data.DataColumnSpecCreator;
import org.knime.core.data.DataRow;
import org.knime.core.data.DataTableSpec;
import org.knime.core.data.collection.CollectionCellFactory;
import org.knime.core.data.collection.ListCell;
import org.knime.core.data.def.DefaultRow;
import org.knime.core.data.def.DoubleCell;
import org.knime.core.data.def.IntCell;
import org.knime.core.data.def.StringCell;
import org.knime.core.node.BufferedDataContainer;
import org.knime.core.node.BufferedDataTable;
import org.knime.core.node.CanceledExecutionException;
import org.knime.core.node.ExecutionContext;
import org.knime.core.node.ExecutionMonitor;
import org.knime.core.node.InvalidSettingsException;
import org.knime.core.node.NodeModel;
import org.knime.core.node.NodeSettingsRO;
import org.knime.core.node.NodeSettingsWO;

class Letter {
	public Letter(String name, int width, int height, float[] data) {
		this.name = name;
		this.width = width;
		this.height = height;
		this.data = data;
	}

	public float[] data;
	public String name;
	public int width;
	public int height;
}

/**
 * This is the model implementation of MuPredictor.
 * 
 * 
 * @author kNN
 */
public class MuPredictorNodeModel extends NodeModel {

	/**
	 * Constructor for the node model.
	 */
	protected MuPredictorNodeModel() {
		super(2, 1);
	}

	private final Collection<Letter> letters = new HashSet<Letter>();
	private int count = 0;

	float[] data = null;
	
	int x = 0;
	int y = 0;
	int rotation = 0;
	int width = 0;
	int height = 0;

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected BufferedDataTable[] execute(final BufferedDataTable[] inData,
			final ExecutionContext exec) throws Exception
	{
		if (letters.size() == 0)
		{
			buildTrainingData(inData[0]);
		}

		loadLetterImage(inData[1]);
		
		// this.setWarningMessage("X:" + x);

		double maxSililarity = 0;
		Letter letter = null;
		float[] d1 = null, d2 = null;
		int w = 0, h = 0;
		HashMap<String, Double> similarities = new HashMap<String, Double>();
		for (Letter curLetter : letters)
		{
			float[] letterData = data;
			float[] curData = curLetter.data;
			int newWidth = width, newHeight = height;
			
			// newWidth = width > curLetter.width? curLetter.width : width;
			// newHeight = height > curLetter.height? curLetter.height : height;
			
			newWidth = width > curLetter.width? curLetter.width : width;
			newHeight = height > curLetter.height? curLetter.height : height;
			
			if (newHeight != curLetter.height || newWidth != curLetter.width)
			{
				curData = scaleImage(curData, curLetter.height, curLetter.width, newHeight, newWidth);
			}
			
			if (newHeight != height || newWidth != width)
			{
				letterData = scaleImage(letterData, height, width, newHeight, newWidth);
			}
			
			double distance = 0;
			int totalPixels = curData.length;
			
			for (int i = 0; i < curData.length; i++)
			{
				/*
				if (curData[i] == 255 && letterData[i] == 255)
				{
					totalPixels--;
					continue;
				}
				*/
				
				distance += (curData[i] - letterData[i]) * (curData[i] - letterData[i]);					
			}
			
			double ratio1 = ((float)curLetter.width) / curLetter.height;
			double ratio2 = ((float)width) / height;
			double factor = ratio1 < ratio2? ratio1 / ratio2 : ratio2 / ratio1;
			
			double similarity = (1 - (Math.sqrt(distance) / totalPixels / 255));
			
			// this.setWarningMessage(curLetter.name + ": " + similarity + " " + ratio1 + " " + ratio2 + " " + factor);
			
			similarity = 0.8 * similarity + 0.2 * factor;
			
			similarities.put(curLetter.name, similarity);

			if (similarity > maxSililarity)
			{
				maxSililarity = similarity;
				letter = curLetter;
				d1 = curData;
				d2 = letterData;
				w = newWidth;
				h = newHeight;
			}
		}
		
		// this.setWarningMessage("----------------------------------------");

		BufferedDataContainer container = exec
				.createDataContainer(getTableSpec());
		
		Collection<DoubleCell> cells1 = new ArrayList<DoubleCell>(d1.length);
		for (float f : d1)
		{
			cells1.add(new DoubleCell(f));
		}
		
		CollectionCellFactory.createListCell(cells1);
		
		Collection<DoubleCell> cells2 = new ArrayList<DoubleCell>(d2.length);
		for (float f : d2)
		{
			cells2.add(new DoubleCell(f));
		}
		
		CollectionCellFactory.createListCell(cells2);
		
		DataCell[] cells = new DataCell[10 + letters.size()];
				
		cells[0] = new StringCell(letter.name);
		cells[1] = new IntCell(x);
		cells[2] = new IntCell(y);
		cells[3] = new DoubleCell(rotation);
		cells[4] = new IntCell(width);
		cells[5] = new IntCell(height);
		cells[6] = CollectionCellFactory.createListCell(cells1);
		cells[7] = CollectionCellFactory.createListCell(cells2);
		cells[8] = new IntCell(w);
		cells[9] = new IntCell(h);
		
		for (Entry<String, Double> entry : similarities.entrySet()) {
			cells[10 + (entry.getKey().charAt(0) - 'A')] = new DoubleCell(entry.getValue());
		}

		DataRow row = new DefaultRow("Row " + ++count, cells);

		container.addRowToTable(row);
		container.close();

		return new BufferedDataTable[] { container.getTable() };
	}

	private void loadLetterImage(BufferedDataTable table)
	{
		int xIndex = table.getSpec().findColumnIndex("x");
		int yIndex = table.getSpec().findColumnIndex("y");
		int pixelIndex = table.getSpec().findColumnIndex("pixel");

		DataColumnSpec xSpec = table.getSpec().getColumnSpec(xIndex);
		DataColumnSpec ySpec = table.getSpec().getColumnSpec(yIndex);

		x = ((IntCell) xSpec.getDomain().getLowerBound()).getIntValue();
		int maxX = ((IntCell) xSpec.getDomain().getUpperBound()).getIntValue();
		y = ((IntCell) ySpec.getDomain().getLowerBound()).getIntValue();
		int maxY = ((IntCell) ySpec.getDomain().getUpperBound()).getIntValue();
		width = maxX - x + 1;
		height = maxY - y + 1;

		data = new float[width * height];

		for (int i = 0; i < data.length; i++)
		{
			data[i] = 255;
		}

		for (DataRow row : table)
		{
			int xx = ((IntCell) row.getCell(xIndex)).getIntValue() - x;
			int yy = ((IntCell) row.getCell(yIndex)).getIntValue() - y;
			int pixel = ((IntCell) row.getCell(pixelIndex)).getIntValue();

			data[xx * height + yy] = pixel;
		}
	}

	private DataTableSpec getTableSpec()
	{
		DataColumnSpec[] cols = new DataColumnSpec[26 + 10];	

		cols[0] = new DataColumnSpecCreator("letter",	StringCell.TYPE).createSpec();
		cols[1] = new DataColumnSpecCreator("x", IntCell.TYPE).createSpec();
		cols[2] = new DataColumnSpecCreator("y", IntCell.TYPE).createSpec();
		cols[3] = new DataColumnSpecCreator("rotation", DoubleCell.TYPE).createSpec();
		cols[4] = new DataColumnSpecCreator("width", IntCell.TYPE).createSpec();
		cols[5] = new DataColumnSpecCreator("height", IntCell.TYPE).createSpec();
		cols[6] = new DataColumnSpecCreator("d1", ListCell.getCollectionType(DoubleCell.TYPE)).createSpec();
		cols[7] = new DataColumnSpecCreator("d2", ListCell.getCollectionType(DoubleCell.TYPE)).createSpec();
		cols[8] = new DataColumnSpecCreator("w", IntCell.TYPE).createSpec();
		cols[9] = new DataColumnSpecCreator("h", IntCell.TYPE).createSpec();
		
		for (char i = 0; i < 26; i++)
		{
			cols[10 + i] = new DataColumnSpecCreator(Character.toString((char) ('A' + i)), DoubleCell.TYPE).createSpec();			
		}
		
		return new DataTableSpec(cols);
	}

	public static float[] scaleImage(float[] rawInput, int width, int height,
			int newWidth, int newHeight) {
		float[] rawOutput = new float[newWidth * newHeight];

		// YD compensates for the x loop by subtracting the width back out
		int YD = (height / newHeight) * width - width;
		int YR = height % newHeight;
		int XD = width / newWidth;
		int XR = width % newWidth;
		int outOffset = 0;
		int inOffset = 0;

		for (int y = newHeight, YE = 0; y > 0; y--) {
			for (int x = newWidth, XE = 0; x > 0; x--) {
				rawOutput[outOffset++] = rawInput[inOffset];
				inOffset += XD;
				XE += XR;
				if (XE >= newWidth) {
					XE -= newWidth;
					inOffset++;
				}
			}
			inOffset += YD;
			YE += YR;
			if (YE >= newHeight) {
				YE -= newHeight;
				inOffset += width;
			}
		}

		return rawOutput;
	}

	private void buildTrainingData(BufferedDataTable table) throws Exception
	{
		int letterIndex = table.getSpec().findColumnIndex("Label");
		int widthIndex = table.getSpec().findColumnIndex("Width");
		int heightIndex = table.getSpec().findColumnIndex("Height");
		int dataIndex = table.getSpec().findColumnIndex("GS");

		for (DataRow row : table)
		{
			String letter = ((StringCell) row.getCell(letterIndex)).getStringValue();
			int width = ((IntCell) row.getCell(widthIndex)).getIntValue();
			int height = ((IntCell) row.getCell(heightIndex)).getIntValue();

			float[] data = new float[width * height];

			ListCell cel = (ListCell) row.getCell(dataIndex);
			for (int i = 0, intCounter = 0; i < width; ++i)
			{
				for (int j = 0; j < height; ++j, ++intCounter)
				{
					data[intCounter] += ((DoubleCell) cel.get(intCounter)).getDoubleValue();
				}
			}

			letters.add(new Letter(letter, width, height, data));
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void reset() {
		letters.clear();
		count = 0;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected DataTableSpec[] configure(final DataTableSpec[] inSpecs)
			throws InvalidSettingsException {
		return new DataTableSpec[] { getTableSpec() };
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void saveSettingsTo(final NodeSettingsWO settings) {
		// TODO: generated method stub
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void loadValidatedSettingsFrom(final NodeSettingsRO settings)
			throws InvalidSettingsException {
		// TODO: generated method stub
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void validateSettings(final NodeSettingsRO settings)
			throws InvalidSettingsException {
		// TODO: generated method stub
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void loadInternals(final File internDir,
			final ExecutionMonitor exec) throws IOException,
			CanceledExecutionException {
		// TODO: generated method stub
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void saveInternals(final File internDir,
			final ExecutionMonitor exec) throws IOException,
			CanceledExecutionException {
		// TODO: generated method stub
	}

}
