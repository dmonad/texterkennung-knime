package org.knimelab.ocr.predictor;

import java.io.File;
import java.io.IOException;

import org.knime.core.data.DataColumnSpec;
import org.knime.core.data.DataColumnSpecCreator;
import org.knime.core.data.DataTableSpec;
import org.knime.core.data.DataType;
import org.knime.core.data.container.AbstractCellFactory;
import org.knime.core.data.container.ColumnRearranger;
import org.knime.core.data.def.DoubleCell;
import org.knime.core.data.def.StringCell;
import org.knime.core.data.image.png.PNGImageCell;
import org.knime.core.node.BufferedDataTable;
import org.knime.core.node.CanceledExecutionException;
import org.knime.core.node.ExecutionContext;
import org.knime.core.node.ExecutionMonitor;
import org.knime.core.node.InvalidSettingsException;
import org.knime.core.node.NodeModel;
import org.knime.core.node.NodeSettingsRO;
import org.knime.core.node.NodeSettingsWO;


/**
 * This is the model implementation of Predictor.
 * It compares images
 *
 * @author Kevin Jahns
 */
public class PredictorNodeModel extends NodeModel {       

    private PredictorNodeSettings settings = new PredictorNodeSettings();

    /**
     * Constructor for the node model.
     */
    protected PredictorNodeModel() {
        super(2, 1);
    }
    
    private DataColumnSpec[] getSpecPart(DataTableSpec learnSpec, DataTableSpec dataSpec) {
    	DataColumnSpec[] allColSpecs = null;
        if(settings.giveBackScaledImages()){
        	 allColSpecs = new DataColumnSpec[] {
        			 	new DataColumnSpecCreator("Dataimage scaled", DataType.getType(PNGImageCell.class)).createSpec(),
        	        	new DataColumnSpecCreator("Learnimage scaled", DataType.getType(PNGImageCell.class)).createSpec(),
        	        	new DataColumnSpecCreator("Letter", DataType.getType(StringCell.class)).createSpec(),
        	        	new DataColumnSpecCreator("Propability", DataType.getType(DoubleCell.class)).createSpec(),
        	        }; 	
        } else {
        	allColSpecs = new DataColumnSpec[] {
    	        	new DataColumnSpecCreator("Letter", DataType.getType(StringCell.class)).createSpec(),
    	        	new DataColumnSpecCreator("Propability", DataType.getType(DoubleCell.class)).createSpec()
    	        }; 	
        }
       
        return allColSpecs;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected BufferedDataTable[] execute(final BufferedDataTable[] inData,
            final ExecutionContext exec) throws Exception {     
        
        ColumnRearranger cr = new ColumnRearranger(inData[1].getSpec());
        
        AbstractCellFactory cellFac = 
        		new PredictorCellFactory(exec, settings, inData, true,
        				getSpecPart(inData[0].getSpec(), inData[1].getSpec()));
    	cr.append(cellFac);
    	BufferedDataTable out = exec.createColumnRearrangeTable(inData[1], cr, exec);
        return new BufferedDataTable[]{out};
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void reset() {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected DataTableSpec[] configure(final DataTableSpec[] inSpecs)
            throws InvalidSettingsException {
    	
    	AbstractCellFactory cellFac =  new PredictorCellFactory(null, settings, null, true,
											getSpecPart(inSpecs[0], inSpecs[1]));
    	ColumnRearranger cr = new ColumnRearranger(inSpecs[1]);
    	cr.append(cellFac);
    	return new DataTableSpec[]{ cr.createSpec() };
    }


    /**
     * {@inheritDoc}
     */
    @Override
    protected void saveSettingsTo(final NodeSettingsWO settings)
    {
        this.settings.saveSettings(settings);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void loadValidatedSettingsFrom(final NodeSettingsRO settings)
            throws InvalidSettingsException
    {
        this.settings.loadSettings(settings);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void validateSettings(final NodeSettingsRO settings)
            throws InvalidSettingsException
    {
        this.settings.validateSettings(settings);
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    protected void loadInternals(final File internDir,
            final ExecutionMonitor exec) throws IOException,
            CanceledExecutionException {     
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    protected void saveInternals(final File internDir,
            final ExecutionMonitor exec) throws IOException,
            CanceledExecutionException {       
    }

}

