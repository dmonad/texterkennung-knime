package org.knimelab.ocr.majority;

import org.knime.core.data.StringValue;
import org.knime.core.node.defaultnodesettings.DefaultNodeSettingsPane;
import org.knime.core.node.defaultnodesettings.DialogComponentColumnFilter;

/**
 * <code>NodeDialog</code> for the "MajorityPredictor" Node.
 * 
 *
 * This node dialog derives from {@link DefaultNodeSettingsPane} which allows
 * creation of a simple dialog with standard components. If you need a more 
 * complex dialog please derive directly from 
 * {@link org.knime.core.node.NodeDialogPane}.
 * 
 * @author Team kNN
 */
public class MajorityPredictorNodeDialog extends DefaultNodeSettingsPane
{
	protected final MajorityPredictorNodeSettings settings = new MajorityPredictorNodeSettings();
	
    /**
     * New pane for configuring the MajorityPredictor node.
     */
    @SuppressWarnings("unchecked")
	protected MajorityPredictorNodeDialog()
    {
    	super();
    	
    	addDialogComponent(new DialogComponentColumnFilter(settings.predictions, 0, false, StringValue.class));
    }
}

