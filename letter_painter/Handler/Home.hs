{-# LANGUAGE TupleSections, OverloadedStrings #-}
module Handler.Home where

import Import
import Data.Maybe
import System.Process

-- This is a handler function for the GET request method on the HomeR
-- resource pattern. All of your resource patterns are defined in
-- config/routes
--
-- The majority of the code you will write in Yesod lives in these handler
-- functions. You can spread them across multiple files if you are so
-- inclined, or create a single monolithic file.

{-
passwordConfirmField :: Field Handler FileInfo
passwordConfirmField = Field
    { fieldParse = \rawVals fileInfos-> 
        case rawVals of
            --[] -> return $ Nothing
            (a:_) -> return $ Right $ Just a
            _  -> return $ Right Nothing
    , fieldView = \idAttr nameAttr _ eResult isReq -> [whamlet|
<input id="lefile" name=#{nameAttr} type="file">
|]
    , fieldEnctype = Multipart
    }
-}


{-fileForm = renderDivs $ (newFileAFormReq fileAFormReq) "lefile"-}
fileForm = renderDivs $ fileAFormReq fieldSettings
            where 
             fieldSettings = FieldSettings {
                         fsLabel = "lefile",
                         fsTooltip = Nothing,
                         fsId = Just "lefile",
                         fsName = Just "lefile",
                         fsAttrs = []
                        }


getHomeR :: Handler Html
getHomeR = do
    ((result, formWidget), formEnctype) <- runFormPost fileForm
    {-$ renderDivs
      $ areq passwordConfirmField "Password" Nothing-}
    let submission = Nothing :: Maybe FileInfo
        handlerName = "getHomeR" :: Text
    defaultLayout $ do
        aDomId <- newIdent
        setTitle "Welcome To Yesod!"
        $(widgetFile "homepage")
        addScriptRemote "http://fabricjs.com/js/prism.js"
        addScriptRemote "http://fabricjs.com/js/master.js"
        addScriptRemote "http://fabricjs.com/lib/fabric.js"



postHomeR :: Handler Html
postHomeR = do
    ((result, formWidget), formEnctype) <- runFormPost fileForm 
    {-$ renderDivs
      $ areq passwordConfirmField "Password" Nothing-}
    let handlerName = "postHomeR" :: Text
        submission = case result of
            FormSuccess res -> Just res
            _ -> Nothing
    let (Just fileInfo) = submission
    liftIO $ do
            fileMove fileInfo "./lePics/pic.png"
            runCommand "convert lePics/pic.png  -background white -flatten +matte lePics/pic.png"
	{-
            runCommand "sleep 0.3"
            runCommand "xdotool key Ctrl+a " 
            runCommand "sleep 0.3"
            runCommand "xdotool key F8 "
            runCommand "sleep 0.3"
            runCommand "xdotool key Return "
            runCommand "sleep 0.3"
            runCommand "xdotool key Shift+F7"
	-}

    defaultLayout $ do
        aDomId <- newIdent
        setTitle "Welcome To Yesod!"
        $(widgetFile "homepage")
        addScriptRemote "http://fabricjs.com/js/prism.js"
        addScriptRemote "http://fabricjs.com/js/master.js"
        addScriptRemote "http://fabricjs.com/lib/fabric.js"

