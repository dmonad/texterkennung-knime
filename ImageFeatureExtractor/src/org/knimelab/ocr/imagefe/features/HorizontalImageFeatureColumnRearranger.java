package org.knimelab.ocr.imagefe.features;

import java.awt.Graphics;

import org.knime.core.data.DataTableSpec;
import org.knimelab.ocr.imagefe.ImageFeatureExtractorNodeSettings;

class HorizontalRowInfo extends RowInfo
{
	double w;
}

public class HorizontalImageFeatureColumnRearranger extends
		ImageFeatureColumnRearranger<HorizontalRowInfo>
{
	public HorizontalImageFeatureColumnRearranger(DataTableSpec original,
			ImageFeatureExtractorNodeSettings settings)
	{
		super(original, settings, "Horizontal");
	}

	@Override
	protected void setup(HorizontalRowInfo data)
	{
		data.w = ((double)data.width) / n;
	}

	@Override
	protected void drawDebug(Graphics g, HorizontalRowInfo data)
	{
		for (double x = data.w; x < data.width; x += data.w)
		{
			g.drawLine((int)x, 0, (int)x, data.height);
		}
	}

	@Override
	protected int getBin(int x, int y, double f, HorizontalRowInfo data)
	{
		return (int) (x / data.w);
	}
}
