#letters="ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"
letters="ABCDEFGHIJKLMNOPQRSTUVWXYZ"
nfonts=10

OIFS="${IFS}"
NIFS=$'\n'
IFS="${NIFS}"

rm -rf letters
mkdir letters

fonts=$(convert -list font | sed -n 's/.*\(Font\|glyphs\): *\(.*\)/\2/p' | rl )
ff=0
fc=$(echo "$fonts" | wc -l)
for (( f=1 ; f < fc; f=f+2 )); do
	x=$(( $f + 1 ))
	font=$(echo "$fonts" | head -n $f | tail -n 1)
	file=$(echo "$fonts" | head -n $x | tail -n 1)
	if [ "$( echo "$font" | egrep "jsMath|Kacst|Kedage|Lohit|Mallige|Symbol|ori1Uni|Pothana|Saab|Vemana|Untitled|Dingbats" )" != "" -o "$( echo "$file" | grep ".ttf" )" == ""  ]
	then
		continue
	fi
	echo "$font"
	((ff++))
	if [ "$ff" -eq "$nfonts" ]
	then
		break
	fi
	glyphs=$(perl -MFont::TTF::Font -e '$f=Font::TTF::Font->open($ARGV[0]); print join("\n",@{$f->{"post"}->read->{"VAL"}});' "$file" )
	for (( i=0; i<${#letters}; i++ )); do
		letter=${letters:$i:1}
		glyph="$letter"
		case $letter in
			0) glyph="zero" ;;
			1) glyph="one" ;;
			2) glyph="two" ;;
			3) glyph="three" ;;
			4) glyph="four" ;;
			5) glyph="five" ;;
			6) glyph="six" ;;
			7) glyph="seven" ;;
			8) glyph="eight" ;;
			9) glyph="nine" ;;
		esac
		contained=$(echo "$glyphs"  | grep -c "^${glyph}$")
		if [ "$contained" -eq "1" ]
		then
			echo -n " $glyph"
			convert -size 64x -font "$font" -gravity NorthWest label:"$letter" -channel Black -fill black -trim "letters/$letter-$ff.png"
			ps=$(identify -format "%[label:pointsize]" "letters/$letter-$ff.png")
			convert -pointsize "$ps" -font "$font" -gravity Center label:"$letter " -channel Black -fill black -trim "letters/$letter-$ff.png"
		fi
	done
	echo ""
done
IFS="${OIFS}"
