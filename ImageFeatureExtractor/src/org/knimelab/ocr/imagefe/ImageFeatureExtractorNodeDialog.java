package org.knimelab.ocr.imagefe;

import org.knime.core.data.DataTableSpec;
import org.knime.core.data.image.png.PNGImageValue;
import org.knime.core.node.InvalidSettingsException;
import org.knime.core.node.NodeSettingsRO;
import org.knime.core.node.NotConfigurableException;
import org.knime.core.node.defaultnodesettings.DefaultNodeSettingsPane;
import org.knime.core.node.defaultnodesettings.DialogComponentBoolean;
import org.knime.core.node.defaultnodesettings.DialogComponentButtonGroup;
import org.knime.core.node.defaultnodesettings.DialogComponentColumnNameSelection;
import org.knime.core.node.defaultnodesettings.DialogComponentNumber;
import org.knime.core.node.util.DataValueColumnFilter;

/**
 * <code>NodeDialog</code> for the "RadialFeatureExtractor" Node.
 * 
 *
 * This node dialog derives from {@link DefaultNodeSettingsPane} which allows
 * creation of a simple dialog with standard components. If you need a more 
 * complex dialog please derive directly from 
 * {@link org.knime.core.node.NodeDialogPane}.
 * 
 * @author Team kNN
 */
public class ImageFeatureExtractorNodeDialog extends DefaultNodeSettingsPane
{
	ImageFeatureExtractorNodeSettings settings = new ImageFeatureExtractorNodeSettings();
	
    @SuppressWarnings("unchecked")
	private static final DataValueColumnFilter DATA_VALUE_COLUMN_FILTER = new DataValueColumnFilter(PNGImageValue.class);

	/**
     * New pane for configuring the RadialFeatureExtractor node.
     */
    protected ImageFeatureExtractorNodeDialog()
    {
    	super();   
        
        this.addDialogComponent(
        		new DialogComponentButtonGroup(
        				settings.mode,
        				false,
        				"Mode:",
        				new String[] { "Angular", "Radial", "Horizontal", "Vertical" }
        		)
        );
        
		this.addDialogComponent(
        		new DialogComponentColumnNameSelection(
        				settings.imageColumn,
        				"Image column:", 
        				0,
        				DATA_VALUE_COLUMN_FILTER
        		)
        );
        
        this.addDialogComponent(
        		new DialogComponentNumber(
        				settings.bins,
        				"Number:", 
        				1
        		)
        );
        
        this.addDialogComponent(
        		new DialogComponentBoolean(
        				settings.useBlackCenter,
        				"Use black center"
        		)
        );
    }
    
    @Override
    protected void loadSettingsFrom(final NodeSettingsRO settings,
            final DataTableSpec[] specs) throws NotConfigurableException
    {
        try
        {
            this.settings.loadSettings(settings);
        }
        catch (InvalidSettingsException ex)
        {
            // Ignore errors and use the defaults
        }
    }
}

