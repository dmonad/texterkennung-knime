package org.knimelab.ocr.majority;

import org.knime.core.node.NodeDialogPane;
import org.knime.core.node.NodeFactory;
import org.knime.core.node.NodeView;

/**
 * <code>NodeFactory</code> for the "MajorityPredictor" Node.
 * 
 *
 * @author Team kNN
 */
public class MajorityPredictorNodeFactory 
        extends NodeFactory<MajorityPredictorNodeModel> {

    /**
     * {@inheritDoc}
     */
    @Override
    public MajorityPredictorNodeModel createNodeModel() {
        return new MajorityPredictorNodeModel();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getNrNodeViews() {
        return 0;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public NodeView<MajorityPredictorNodeModel> createNodeView(final int viewIndex,
            final MajorityPredictorNodeModel nodeModel)
    {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasDialog() {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public NodeDialogPane createNodeDialogPane() {
        return new MajorityPredictorNodeDialog();
    }

}

