package org.knimelab.ocr.predictor;

import java.awt.Image;

import org.knime.core.node.InvalidSettingsException;
import org.knime.core.node.NodeSettingsRO;
import org.knime.core.node.NodeSettingsWO;
import org.knime.core.node.defaultnodesettings.SettingsModelBoolean;
import org.knime.core.node.defaultnodesettings.SettingsModelDouble;
import org.knime.core.node.defaultnodesettings.SettingsModelDoubleBounded;
import org.knime.core.node.defaultnodesettings.SettingsModelString;
import org.knime.core.node.defaultnodesettings.SettingsModelStringArray;

public class PredictorNodeSettings {
	
	public final SettingsModelStringArray scaleImagesBy = 
			new SettingsModelStringArray("Skale images By", new String[]{"Height","Width"});
	
	public final SettingsModelBoolean giveBackScaledImages = 
			new SettingsModelBoolean("Give back scaled images", false);
	
    public final SettingsModelBoolean scaleUp = 
    		new SettingsModelBoolean("Scale up (otherwise scale down)", false);
    
    public final SettingsModelString scaleFunction = 
    		new SettingsModelString("Scale function", "Default");
    
    public final SettingsModelStringArray similarityFunction = 
    		new SettingsModelStringArray("Similarity function", new String[]{"Cosine similarity (to black)"});
    
    public final SettingsModelString learnerImageName =
    		new SettingsModelString("Learner image column", "Image");
    
    public final SettingsModelString dataImageName = 
    		new SettingsModelString("Data image column", "Image");
    
    public final SettingsModelString learnerImageLetter = 
    		new SettingsModelString("Letter representing lerner image", "Letter");
    
    // Learning port projection
    final int maxProjectionModels = 9;
    
    final SettingsModelString[] addProjectionLearningModel = 
    		new SettingsModelString[maxProjectionModels];
    final SettingsModelString[] addProjectionDataModel = 
    		new SettingsModelString[maxProjectionModels];
    final SettingsModelDouble[] distanceStepModel = 
    		new SettingsModelDouble[maxProjectionModels];
    final SettingsModelBoolean[] addProjectionButtonModel = 
    		new SettingsModelBoolean[maxProjectionModels];
   	final SettingsModelDoubleBounded takeAtLeastRelative =
   			new SettingsModelDoubleBounded("take at least relative", 0.001, 0,1);
   	final SettingsModelDoubleBounded minimumPropability = 
   			new SettingsModelDoubleBounded("minimum propability", 0, 0, 1);
   	
   	// end Learning port projection
   	
   	protected PredictorNodeSettings(){
   		for(int i=0;i<maxProjectionModels; i++){
   			addProjectionDataModel[i] 
        			= new SettingsModelString("data projection "+i, "Null");
        	addProjectionLearningModel[i] 
        			= new SettingsModelString("learning projection "+i, "Null");
        	distanceStepModel[i] = new SettingsModelDouble("stepsize "+i, 0); 
        	addProjectionButtonModel[i] = new SettingsModelBoolean("more projection "+i, false);
   		}
   	}
   	
    public String getLearnerImageLetter(){
    	return learnerImageLetter.getStringValue();
    }
    
    public String getDataImageName(){
    	return dataImageName.getStringValue();
    }
    
    public String getLearnerImageName(){
    	return learnerImageName.getStringValue();
    }
    
    public String getSimilarityFunction(){
    	return similarityFunction.getStringArrayValue()[0];
    }
    
    public boolean isCurrentSimilarityFunction(String f){
    	return similarityFunction.getStringArrayValue()[0].contains(f);
    }

    public int getScaleFunction() throws Exception{
		if(scaleFunction.getStringValue().contains("Area averaging")){
    		return Image.SCALE_AREA_AVERAGING;
    	} else if(scaleFunction.getStringValue().contains("Fast")){
    		return Image.SCALE_FAST;
    	} else if(scaleFunction.getStringValue().contains("Default")){
    		return Image.SCALE_DEFAULT;
    	} else if(scaleFunction.getStringValue().contains("Replicate")){
    		return Image.SCALE_REPLICATE;
    	} else if(scaleFunction.getStringValue().contains("Smooth")){
    		return Image.SCALE_SMOOTH;
    	} else {
    		throw new Exception("Don't know scale Function: " + scaleFunction.getStringValue() );
    	}
    }

    public void saveSettings(final NodeSettingsWO settings)
    {
    	learnerImageLetter.saveSettingsTo(settings);    	
    	dataImageName.saveSettingsTo(settings);
    	learnerImageName.saveSettingsTo(settings);
        scaleImagesBy.saveSettingsTo(settings);
        giveBackScaledImages.saveSettingsTo(settings);
        scaleUp.saveSettingsTo(settings);
        scaleFunction.saveSettingsTo(settings);
        similarityFunction.saveSettingsTo(settings);
        
        for(int i=0; i<maxProjectionModels; i++){
        	addProjectionLearningModel[i].saveSettingsTo(settings);
       		addProjectionDataModel[i].saveSettingsTo(settings);
       		distanceStepModel[i].saveSettingsTo(settings);
       		addProjectionButtonModel[i].saveSettingsTo(settings);
        }
   		takeAtLeastRelative.saveSettingsTo(settings);
   		minimumPropability.saveSettingsTo(settings);

    }

    public void loadSettings(final NodeSettingsRO settings)
            throws InvalidSettingsException
    {
    	learnerImageLetter.loadSettingsFrom(settings);
    	dataImageName.loadSettingsFrom(settings);
    	learnerImageName.loadSettingsFrom(settings);
        scaleImagesBy.loadSettingsFrom(settings);
        giveBackScaledImages.loadSettingsFrom(settings);
        scaleUp.loadSettingsFrom(settings);
        scaleFunction.loadSettingsFrom(settings);
        similarityFunction.loadSettingsFrom(settings);
        
        for(int i = 0; i<maxProjectionModels; i++){
        	addProjectionLearningModel[i].loadSettingsFrom(settings);
       		addProjectionDataModel[i].loadSettingsFrom(settings);
       		distanceStepModel[i].loadSettingsFrom(settings);
       		addProjectionButtonModel[i].loadSettingsFrom(settings);
        }
   		takeAtLeastRelative.loadSettingsFrom(settings);
   		minimumPropability.loadSettingsFrom(settings);
    }

    public void validateSettings(final NodeSettingsRO settings)
            throws InvalidSettingsException
    {
    	learnerImageLetter.validateSettings(settings);
    	dataImageName.validateSettings(settings);
    	learnerImageName.validateSettings(settings);
        scaleImagesBy.validateSettings(settings);
        giveBackScaledImages.validateSettings(settings);
        scaleUp.validateSettings(settings);
        scaleFunction.loadSettingsFrom(settings);
        similarityFunction.loadSettingsFrom(settings);
        
        for(int i=0; i<maxProjectionModels; i++){
        	addProjectionLearningModel[i].validateSettings(settings);
       		addProjectionDataModel[i].validateSettings(settings);
       		distanceStepModel[i].validateSettings(settings);
       		addProjectionButtonModel[i].validateSettings(settings);
        }
        takeAtLeastRelative.validateSettings(settings);
        minimumPropability.validateSettings(settings);
    }
    
    public boolean scaleUp(){
    	return scaleUp.getBooleanValue();
    }
    
	public boolean giveBackScaledImages() {
		return giveBackScaledImages.getBooleanValue();
	}
	
    public boolean scaleImagesByHeight(){
    	for( String v : scaleImagesBy.getStringArrayValue()){
    		if(v == "Height"){
    			return true;
    		}
    	}
    	return false;
    }
    
    public boolean scaleImagesByWidth(){
    	for( String v : scaleImagesBy.getStringArrayValue()){
    		if(v == "Width"){
    			return true;
    		}
    	}
    	return false;
    }	
    


}
